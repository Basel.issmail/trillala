<?php
/**
 * Created by PhpStorm.
 * User: basel
 * Date: 09/10/17
 * Time: 11:22 م
 */

namespace TrillalaBundle\Tests\Api;

use Tests\ApiTestCase;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class RegistrationControllerTest extends ApiTestCase
{
    public function testPostRegsiterNewUser()
    {
        $data = [
            'username' => 'matko',
            'email' => 'matko@gmail.com',
            'plainPassword' => [
                'first' => 'test123', 'second' => 'test123'
            ]
        ];

        $response = $this->client->request(
            'POST', '/api/register',[
            'json' => $data]
        );

        $this->assertEquals(
            201, $response->getStatusCode()
        );
    }
}