<?php

namespace Tests\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Tests\ApiTestCase;

class DefaultControllerTest  extends ApiTestCase
{
    public function testPOST()
    {
        $nickname = 'ObjectOrienter'.rand(0, 999);
        $data = array(
            'nickname' => $nickname,
            'avatarNumber' => 5,
            'tagLine' => 'a test dev!'
        );
        // 1) Create a programmer resource
        $response = $this->client->post('/api/home', [
            'body' => json_encode($data)
        ]);

        $this->assertEquals(201, $response->getStatusCode());

    }

}
