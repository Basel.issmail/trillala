<?php
/**
 * Created by PhpStorm.
 * User: basel
 * Date: 14/10/17
 * Time: 07:30 م
 */

namespace TrillalaBundle\Form;

use Symfony\Component\Form\ChoiceList;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use FOS\UserBundle\Form\Type\RegistrationFormType as BaseRegistrationFormType;


class RegistrationFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('gender', null, [
                'required' => true,
                ])->add('country', null, [
                'required' => true,
            ]);
    }
    public function getParent()
    {
        return BaseRegistrationFormType::class;
    }
}