<?php
/**
 * Created by PhpStorm.
 * User: basel
 * Date: 29/10/17
 * Time: 11:40 ص
 */

namespace TrillalaBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class SettingsFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('receiveFromMale')
            ->add('receiveFromFemale')
            ->add('notification')
            ->add('maxContacts')
            ->add('maxMessages')
            ->add('maxTime')
            ->add('extraMessages')
            ->add('receiveCountry', 'checkbox', array('property_path' => 'receiveFromCountry'))
        ;
    }
}