<?php
/**
 * Created by PhpStorm.
 * User: basel
 * Date: 15/12/17
 * Time: 07:55 م
 */

namespace TrillalaBundle\Service;

use Doctrine\ORM\EntityManager;
use TrillalaBundle\Entity\Settings;
use TrillalaBundle\Entity\User;
use TrillalaBundle\Entity\Revenue;

class RevenueService {
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function checkPurchase(Settings $oldSettings, Settings $newSettings, User $user, $avatarPurchase = false){
        if($avatarPurchase){
            if($avatarPurchase == 'm') $this->setRevenue($user, 'male_avatars_package', 0.99);
            else if($avatarPurchase == 'f') $this->setRevenue($user, 'female_avatars_package', 0.99);
            else $this->setRevenue($user, $avatarPurchase, 0.99);
            return;
        }
        if( $newSettings->getMaxTime() != $oldSettings->getMaxTime()){
            if($newSettings->getMaxTime() == 8) $this->setRevenue($user, '8_seconds', 2.99);
            if($newSettings->getMaxTime() == 12) $this->setRevenue($user, '12_seconds', 4.99);
        }
        if($newSettings->getMaxContacts() != $oldSettings->getMaxContacts()){
            if($newSettings->getMaxContacts() == 12) $this->setRevenue($user, '12_contacts', 1.99);
            if($newSettings->getMaxContacts() > 1000) $this->setRevenue($user, 'unlimited_contacts', 5.99);
        }
        if($newSettings->getMaxMessages() != $oldSettings->getMaxMessages()){
            if($newSettings->getMaxMessages() == 6) $this->setRevenue($user, '6_daily_messages', 0.99);
            if($newSettings->getMaxMessages() == 10) $this->setRevenue($user, '10_daily_messages', 1.99);
            if($newSettings->getMaxMessages() > 1000) $this->setRevenue($user, '10_daily_messages', 3.99);
        }

    }

    private function setRevenue(User $user, $code, $price){
        $revenue = new Revenue();
        $revenue->setUser($user);
        $revenue->setDetails($code);
        $revenue->setPrice($price);
        $this->em->persist($revenue);
        $this->em->flush();
    }

    private function getPrice($code){
        if($code === '8_seconds') return 2.99;
        if($code === 'female_avatars_package' || $code === 'male_avatars_package' || $code === '6_daily_messages') return 0.99;
        if($code === '10_daily_messages' || $code === '12_contacts') return 1.99;
        if($code === '12_seconds') return 4.99;
        if($code === 'unlimited_contacts') return 3.99;
        if($code === 'unlimited_messages') return 5.99;
        return 0;
     }

}