<?php
/**
 * Created by PhpStorm.
 * User: basel
 * Date: 15/12/17
 * Time: 07:55 م
 */

namespace TrillalaBundle\Service;

use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\JsonResponse;
use TrillalaBundle\Entity\User;
use Lexik\Bundle\JWTAuthenticationBundle\Encoder\JWTEncoderInterface;
use Lexik\Bundle\JWTAuthenticationBundle\TokenExtractor\AuthorizationHeaderTokenExtractor;

class UserInfo {

    public function __construct(EntityManager $em, JWTEncoderInterface $lexik_jwt_authentication)
    {
        $this->em = $em;
        $this->lexik_jwt_authentication = $lexik_jwt_authentication;
    }

    public function setUser(User $user){
        $this->user = $user;
        $this->settings = $this->em
            ->getRepository('TrillalaBundle:Settings')->findOneByUser($this->user);
    }

    public function getUserInfo(){
        $username = $this->user->getUsername();
        $email = $this->user->getEmail();
        $gender = $this->user->getGender();
        $status = $this->user->getStatus();
        $avatar = $this->user->getAvatar();
        $fullname = $this->user->getFullname();

        $facebook = ($this->user->getFacebookUsername())? true: false;

        $settings = array( 'receiveFromMale' => $this->settings->getReceiveFromMale(),'receiveFromFemale' => $this->settings->getReceiveFromFemale(), 'facebook' => $facebook, 'receiveCountry' => $this->settings->getReceiveFromCountry(), 'receiveNotifications' => $this->settings->getNotification(), 'maxTime' => $this->settings->getMaxTime(), 'maxContacts' => $this->settings->getMaxContacts(), 'maxMessages' => $this->settings->getMaxMessages() );

        return ['username' => $username,'fullname' => $fullname, 'email' => $email , 'gender' => $gender, "avatar" => $avatar ,"day_message" => $status,'remainingMessages' => $this->getRemainingMessages() , 'remainingContacts' => $this->getRemainingContacts(),'settings' => $settings , 'status' => 200];
    }

    private function getUserToken(){
        $token = $this->lexik_jwt_authentication
            ->encode([
                'username' => $this->user->getUsername()
            ]);
        return $token;
    }

    public function addTokenInResponse($response){
        $token = $this->getUserToken();
        $response['token'] = $token;
        return $response;
    }

    public function getRemainingMessages(){
        $remainingMessages = $this->getRemainingMessagesNoExtra();
        $userCurrentExtra = $this->settings->getExtraMessages();
        return ($remainingMessages < 0)? $userCurrentExtra : $userCurrentExtra + $remainingMessages;
    }

    public function getRemainingContacts(){
        $remainingContacts = $this->em
            ->getRepository('TrillalaBundle:Contact')->countContacts($this->user);

        return ($this->settings->getMaxContacts() < 1000 )?  $this->settings->getMaxContacts()  - $remainingContacts[0]['total'] :  $this->settings->getMaxContacts();
    }

    public function getRemainingMessagesNoExtra(){
        $yesterday = new \DateTime();
        $remainingMessages = $this->em
            ->getRepository('TrillalaBundle:Message')->getRemainingMessages($this->user, $yesterday);
        return ($this->settings->getMaxMessages() < 1000) ? $this->settings->getMaxMessages() - $remainingMessages[0]['total'] : $this->settings->getMaxMessages();
    }

    public function tryUseExtraMessages(){
        if($this->getRemainingMessagesNoExtra() < 0){
            $extra = $this->settings->getExtraMessages();
            $extra--;
            $this->settings->setExtraMessages($extra);
            $this->em->persist($this->settings);
            $this->em->flush();
        }
    }
}