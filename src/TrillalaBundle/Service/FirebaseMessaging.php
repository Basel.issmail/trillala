<?php
/**
 * Created by PhpStorm.
 * User: basel
 * Date: 29/10/17
 * Time: 08:05 م
 */
namespace TrillalaBundle\Service;

use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Translation\Translator;

class FirebaseMessaging
{
    const KEY = 'AAAA7CRSpAk:APA91bE3o3gitxJSJcNMX6sup66f3E2cjbTgMgheFVnAI7k8Qg4QxVcTRBJn6YNkezFtPCwk2N6rTOd2fYqLCA0qywi6BgUFoC3jIvhqcZJ90UnW2NqXQgrcUYyon4kFLXRCqP2-nnzJ';
    const URL ='https://android.googleapis.com/gcm/send';

    public function sendMessage($gcmId,$message, $name)
    {

        $dt=time();
        $dt=date("Y-m-d H:m:s",$dt);
        //$message = array("title" => $title,"message"=>$description,"timestamp"=>$dt);
        $fields = array(
            'registration_ids' => array($gcmId),
            'data' => $message,
            'notification' => array('title'=> 'New Message', 'body' => 'You have a new message from ' . $name ),
            'content_available' => true,
            'priority' => 'high'
        );

        $headers = array(
            'Authorization: key=' . self::KEY,
            'Content-Type: application/json'
        );
        // Open connection
        $ch = curl_init();

        // Set the url, number of POST vars, POST data
        curl_setopt($ch, CURLOPT_URL, self::URL);

        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        // Disabling SSL Certificate support temporarly
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);
        if ($result === FALSE) {
            throw new \Exception("couldn't send message" .curl_error($ch));
        }
        //dump($result);die();
        $result = json_decode($result);
        if($result->failure == 1){
            curl_close($ch);
            return false;
        }
//        print_r($result);
       // dump($result);die();
        curl_close($ch);
        return true;

    }

}