<?php
/**
 * Created by PhpStorm.
 * User: basel
 * Date: 12/10/17
 * Time: 03:21 ص
 */

namespace TrillalaBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;


class MessageRepository extends EntityRepository
{
    public function getConversationLastMessage($user, $conversation)
    {
        return $this->getEntityManager()
            ->createQueryBuilder()
            ->select('m.id, m.content, m.type, m.seen, m.datetime')
            ->from('TrillalaBundle:Message', 'm')
            ->innerJoin('m.conversation', 'c')
            ->where('c.user0 = :user0')
            ->orWhere('c.user1 = :user0')
            ->andWhere('m.conversation = :conversation')
            ->andWhere('m.receiver = :user0')
            ->setParameter('user0', $user)
            ->setParameter('conversation', $conversation)
            ->orderBy('m.datetime', 'desc')
            ->setMaxResults(1)
            ->getQuery()
            ->getResult();
    }

    public function getMessagesGroupByDayMonth()
    {
        return $this->getEntityManager()
            ->createQueryBuilder()
            ->select('COUNT(m.id), DAY(m.datetime) as message_date')
            ->from('TrillalaBundle:Message', 'm')
            ->orderBy('message_date', 'desc')
            ->groupBy('message_date')
            ->getQuery()
            ->getResult();
    }

    public function getRemainingMessages($user, $yesterday)
    {
        $type = 'voice';
        return $this->getEntityManager()
            ->createQueryBuilder()
            ->select('COUNT(m.id) total')
            ->from('TrillalaBundle:Message', 'm')
            ->where('user = :user')
            ->andWhere('DATE(m.datetime) = Date(:yesterday)')
            ->andWhere('m.type = :type')
            ->setParameter('user', $user)
            ->setParameter('yesterday', $yesterday)
            ->setParameter('type', $type)
            ->innerJoin('m.user', 'user')
            ->setMaxResults(1)
            ->getQuery()
            ->getResult();
    }

    public function countMessageByDate($date)
    {
        return $query = $this->getEntityManager()
            ->createQueryBuilder()
            ->select('Count(m.id) total, DAY(m.datetime) ddate,MONTH(m.datetime) mdate, YEAR(m.datetime) ydate')
            ->from('TrillalaBundle:Message', 'm')
            ->where('MONTH(m.datetime) <= :mdate')
            ->setParameter('mdate', $date)
            ->groupBy('ddate')
            ->addGroupBy('mdate')
            ->addGroupBy('ydate')
            ->getQuery()
            ->getResult();
    }

    public function getAllReceivedMessages($user, $page, $size)
    {
        return $this->getEntityManager()
            ->createQueryBuilder()
            ->select('m.id, user.username sender, user.fullname fullname, user.gender gender, user.avatar avatar, m.content, m.type, m.seen, m.datetime')
            ->from('TrillalaBundle:Message', 'm')
            ->where('m.receiver = :user')
            ->andWhere('m.deleted = false')
            ->innerJoin('m.user', 'user')
            ->setParameter('user', $user)
            ->orderBy('m.datetime', 'desc')
            ->setFirstResult($page)
            ->setMaxResults($size)
            ->getQuery()
            ->getResult();
    }

    public function CountAllReceivedMessages($user){
        return $this->getEntityManager()
            ->createQueryBuilder()
            ->select('COUNT(m.id) total')
            ->from('TrillalaBundle:Message', 'm')
            ->where('m.receiver = :user')
            ->andWhere('m.deleted = false')
            ->setParameter('user', $user)
            ->setMaxResults(1)
            ->getQuery()
            ->getResult();
    }

    public function CountAllSentMessages($user){
        return $this->getEntityManager()
            ->createQueryBuilder()
            ->select('COUNT(m.id) total')
            ->from('TrillalaBundle:Message', 'm')
            ->where('m.user = :user')
            ->setParameter('user', $user)
            ->setMaxResults(1)
            ->getQuery()
            ->getResult();
    }

    public function countAllMessages()
    {
        return $query = $this->getEntityManager()
            ->createQueryBuilder()
            ->select('Count(m.id) total')
            ->from('TrillalaBundle:Message', 'm')
            ->setMaxResults(1)
            ->getQuery()
            ->getResult();
    }
}