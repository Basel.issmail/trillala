<?php
/**
 * Created by PhpStorm.
 * User: basel
 * Date: 11/10/17
 * Time: 03:39 م
 */

namespace TrillalaBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;


class NotificationRepository extends EntityRepository
{
    /**
     * Get Last not deleted notification.
     *
     * @return array
     *
     * @author Basel Ismail
     */
    public function getLastNotification()
    {

        return $this->getEntityManager()->createQueryBuilder()
            ->select("notification")
            ->from("TrillalaBundle:Notification", "notification")
            ->where("notification.deleted = 0")
            ->orderBy("notification.startTime", 'desc')
            ->setMaxResults(1)
            ->getQuery()->getOneOrNullResult();
    }

    /**
     * Get Last not deleted notification.
     *
     * @return array
     *
     * @author Basel Ismail
     */
    public function getNewNotifications()
    {

        return $this->getEntityManager()->createQueryBuilder()
            ->select("notification.id, notification.title , notification.body, notification.createTime")
            ->from("TrillalaBundle:Notification", "notification")
            ->where("notification.deleted = 0")
            ->andWhere("notification.send = 1")
            ->orderBy("notification.createTime")
            ->getQuery()->getResult();
    }
}