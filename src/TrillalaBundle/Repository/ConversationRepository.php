<?php
/**
 * Created by PhpStorm.
 * User: basel
 * Date: 12/10/17
 * Time: 01:10 ص
 */

namespace TrillalaBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;
use TrillalaBundle\Entity\Conversation;


class ConversationRepository extends EntityRepository
{

    /*
     * return conversation or create one if does not exist
     */
    public function getConversation($me, $him, $oneDirection = false)
    {
        $query = $this->getEntityManager()->getRepository('TrillalaBundle:Conversation')
            ->createQueryBuilder('c')
            ->select('c')
            ->where('c.user0 = :user0 AND c.user1 = :user1')
            ->setParameter('user0', $me)
            ->setParameter('user1', $him);
        if (!$oneDirection) {
            $query->orWhere('c.user1 = :user0 AND c.user0 = :user1')
                ->setParameter('user0', $me)
                ->setParameter('user1', $him);
        }

        $query = $query->getQuery()
            ->setMaxResults(1);
        $conversations = $query->getResult();
        if (count($conversations) === 0) {

            //increase recieved messages by him
            $numberOfConvs = $me->getConversationNum() + 1;
            $me = $me->setConversationNum($numberOfConvs);
            $this->getEntityManager()->persist($me);


            $numberOfConvs = $him->getConversationNum() + 1;
            $him = $him->setConversationNum($numberOfConvs);
            $this->getEntityManager()->persist($him);


            $conversation = new Conversation();
            $conversation->setUser0($me);
            $conversation->setUser1($him);
            $this->getEntityManager()->persist($conversation);
            $this->getEntityManager()->flush();
        } else {
            $conversation = $conversations[0];
        }
        return $conversation;
    }
    /*
     * sorted by date
     */
    public function getAllConversation($user){
        return $this->getEntityManager()->getRepository('TrillalaBundle:Conversation')
            ->createQueryBuilder('c')
            ->select('c.id, u0.username user1, u0.fullname fullname1, u0.gender gender1, u1.username user2, u1.fullname fullname2, u1.gender gender2, c.datetime ')
            ->where('c.user0 = :user0')
            ->setParameter('user0', $user)
            ->orWhere('c.user1 = :user0')
            ->innerJoin('c.user0', 'u0')
            ->innerJoin('c.user1', 'u1')
            ->setParameter('user0', $user)
            ->orderBy('c.datetime','desc')
            ->getQuery()
            ->getResult();
    }

    public function getIncomingConMessages($user){
        return $this->getEntityManager()
            ->createQueryBuilder()
            ->select('c.id, u0.username user1, u0.fullname fullname1, u0.gender gender1, u1.username user2, u1.fullname fullname2, u1.gender gender2, c.datetime')
            ->from('TrillalaBundle:Conversation', 'c')
            ->where('m.receiver = :receiver')
            ->setParameter('receiver', $user)
            ->innerJoin('c.messages', 'm')
            ->innerJoin('c.user0', 'u0')
            ->innerJoin('c.user1', 'u1')
            ->having('COUNT(m.id) = 1')
            ->groupBy('c.id')
            ->getQuery()
            ->getResult();
    }

    public function getAllReceivedConversations($user){
        return $this->getEntityManager()
            ->createQueryBuilder()
            ->select('c.id, u0.username user1, u0.fullname fullname1 u0.gender gender1, u1.username user2, u1.fullname fullname2, u1.gender gender2, c.datetime')
            ->from('TrillalaBundle:Conversation', 'c')
            ->where('m.receiver = :receiver')
            ->setParameter('receiver', $user)
            ->innerJoin('c.messages', 'm')
            ->innerJoin('c.user0', 'u0')
            ->innerJoin('c.user1', 'u1')
            ->getQuery()
            ->getResult();
    }
//CASE WHEN (c.parent IS NULL) THEN c.name ELSE 'something' END
    public function getContactConversations($user, $contacts){
        return $this->getEntityManager()
            ->createQueryBuilder()
            //->select('c.id, u0.username user1, u0.fullname fullname1, u0.gender gender1, u1.username user2, u1.fullname fullname2, u1.gender gender2, c.datetime')
            ->select('c.id conversation, CASE WHEN (u1 = :user and u0.username IN (:contacts) ) THEN u0.username ELSE u1.username END as username, 
            CASE WHEN (u1 = :user and u0.username IN (:contacts) ) THEN u0.fullname ELSE u1.fullname END as fullname, 
            CASE WHEN (u1 = :user and u0.username IN (:contacts) ) THEN u0.gender ELSE u1.gender END as gender, 
            CASE WHEN (u1 = :user and u0.username IN (:contacts) ) THEN u0.avatar ELSE u1.avatar END as avatar')
            ->from('TrillalaBundle:Conversation', 'c')
            ->where('u0 = :user and u1.username IN (:contacts)')
            ->orWhere('u1 = :user and u0.username IN (:contacts)')
            ->setParameter('user', $user)
            ->setParameter('contacts', $contacts)
            ->innerJoin('c.user0', 'u0')
            ->innerJoin('c.user1', 'u1')
            ->getQuery()
            ->getResult();
    }


    /*
    * return conversation or create one if does not exist
    */
    public function getConversationWithoutIncreasing($me, $him, $oneDirection = false)
    {
        $query = $this->getEntityManager()->getRepository('TrillalaBundle:Conversation')
            ->createQueryBuilder('c')
            ->select('c')
            ->where('c.user0 = :user0 AND c.user1 = :user1')
            ->setParameter('user0', $me)
            ->setParameter('user1', $him);
        if (!$oneDirection) {
            $query->orWhere('c.user1 = :user0 AND c.user0 = :user1')
                ->setParameter('user0', $me)
                ->setParameter('user1', $him);
        }

        $query = $query->getQuery()
            ->setMaxResults(1);
        $conversations = $query->getResult();
        if (count($conversations) === 0) {

            /*//increase recieved messages by him
            $numberOfConvs = $me->getConversationNum() + 1;
            $me = $me->setConversationNum($numberOfConvs);
            $this->getEntityManager()->persist($me);


            $numberOfConvs = $him->getConversationNum() + 1;
            $him = $him->setConversationNum($numberOfConvs);
            $this->getEntityManager()->persist($him);*/


            $conversation = new Conversation();
            $conversation->setUser0($me);
            $conversation->setUser1($him);
            $this->getEntityManager()->persist($conversation);
            $this->getEntityManager()->flush();
        } else {
            $conversation = $conversations[0];
        }
        return $conversation;
    }

}