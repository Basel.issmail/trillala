<?php
/**
 * Created by PhpStorm.
 * User: basel
 * Date: 09/11/17
 * Time: 02:25 م
 */


namespace TrillalaBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;
use TrillalaBundle\Entity\Conversation;


class AvatarRepository extends EntityRepository
{
    public function getAllPurchasedAvatars($user)
    {
        return $this->getEntityManager()
            ->createQueryBuilder()
            ->select('a.avatar')
            ->from('TrillalaBundle:Avatar', 'a')
            ->where('a.user = :user')
            ->setParameter('user', $user)
            ->getQuery()
            ->getResult();
    }
}