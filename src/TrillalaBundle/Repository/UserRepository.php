<?php
/**
 * Created by PhpStorm.
 * User: basel
 * Date: 12/10/17
 * Time: 12:38 ص
 */
namespace TrillalaBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;


class UserRepository extends EntityRepository
{
    public function getUserToSend($user, $randomNumber, $sortReceived, $sortLastLogin)
    {
        $query = $this->getEntityManager()
            ->createQueryBuilder()
            ->select('u')
            ->from('TrillalaBundle:User', 'u')
            ->where('u != :currentUser')
            ->andWhere('u.enabled = true')
            ->andWhere('u.gcmToken is not null')
            ->andWhere('u.username != :admin')
            ->innerJoin('u.settings', 's')
            ->setParameter('currentUser', $user)
            ->setParameter('admin', 'admin')
            ->andWhere('s.notification = true');

        if ($user->getGender() == 'm') {
            $query->andWhere('((s.receiveFromMale = true and s.receiveFromFemale = false)
            or (s.receiveFromMale = false and s.receiveFromFemale = false)
            or (s.receiveFromMale = true and s.receiveFromFemale = true))');
        } else {
            $query->andWhere('((s.receiveFromFemale = true and s.receiveFromMale = false)
             or (s.receiveFromMale = false and s.receiveFromFemale = false)
            or (s.receiveFromMale = true and s.receiveFromFemale = true))');
        }
        $query->andWhere('(s.receiveFromCountry = true and u.country = :userCountry) or s.receiveFromCountry = false ')
        ->setParameter('userCountry', $user->getCountry());

        if($sortReceived == 'asc') $query->orderBy('u.randomMessagesReceived', 'asc');
        else if($sortReceived == 'desc') $query->orderBy('u.randomMessagesReceived', 'desc');

        if($sortLastLogin == 'asc') $query->addOrderBy('u.lastLogin', 'asc');
        else $query->addOrderBy('u.lastLogin', 'desc');

        return $query->setMaxResults(1)
            ->setFirstResult($randomNumber)
            ->getQuery()
            ->getResult();

    }

    public function getLeastFortunateUserCount($user)
    {
        $query = $this->getEntityManager()
            ->createQueryBuilder()
            ->select('Count(u.id) total')
            ->from('TrillalaBundle:User', 'u')
            ->where('u != :currentUser')
            ->andWhere('u.enabled = true')
            ->andWhere('u.gcmToken is not null')
            ->andWhere('u.username != :admin')
            ->innerJoin('u.settings', 's')
            ->setParameter('currentUser', $user)
            ->setParameter('admin', 'admin')
            ->andWhere('s.notification = true');

        if ($user->getGender() == 'm') {
            $query->andWhere('((s.receiveFromMale = true and s.receiveFromFemale = false)
            or (s.receiveFromMale = false and s.receiveFromFemale = false)
            or (s.receiveFromMale = true and s.receiveFromFemale = true))');
        } else {
            $query->andWhere('((s.receiveFromFemale = true and s.receiveFromMale = false)
             or (s.receiveFromMale = false and s.receiveFromFemale = false)
            or (s.receiveFromMale = true and s.receiveFromFemale = true))');
        }
        $query->andWhere('(s.receiveFromCountry = true and u.country = :userCountry) or s.receiveFromCountry = false ')
            ->setParameter('userCountry', $user->getCountry());

        return $query->orderBy('u.randomMessagesReceived', 'asc')
            ->addOrderBy('u.lastLogin', 'desc')
            ->setMaxResults(1)
            ->getQuery()
            ->getResult();

    }


    public function countUsersByGender($gender)
    {
        return $query = $this->getEntityManager()
            ->createQueryBuilder()
            ->select('Count(u.id) total')
            ->from('TrillalaBundle:User', 'u')
            ->where('u.gender = :gender')
            ->setParameter('gender', $gender)
            ->setMaxResults(1)
            ->getQuery()
            ->getResult();
    }

    public function countUsersByLoginDate($date)
    {
        return $query = $this->getEntityManager()
            ->createQueryBuilder()
            ->select('Count(u.id) total')
            ->from('TrillalaBundle:User', 'u')
            ->where('u.lastLogin >= :date')
            ->setParameter('date', $date)
            ->setMaxResults(1)
            ->getQuery()
            ->getResult();
    }

    public function countAllUsers()
    {
        return $query = $this->getEntityManager()
            ->createQueryBuilder()
            ->select('Count(u.id) total')
            ->from('TrillalaBundle:User', 'u')
            ->setMaxResults(1)
            ->getQuery()
            ->getResult();
    }

    public function countUsersByRegion()
    {
        return $query = $this->getEntityManager()
            ->createQueryBuilder()
            ->select('Count(u.id) total, u.country region')
            ->from('TrillalaBundle:User', 'u')
            ->where('u.country is not null')
            ->groupBy('region')
            ->setMaxResults(3)
            ->getQuery()
            ->getResult();
    }

    public function topFiveConversationUsers()
    {
        return $query = $this->getEntityManager()
            ->createQueryBuilder()
            ->select('u.username, u.conversationNum')
            ->from('TrillalaBundle:User', 'u')
            ->orderBy('u.conversationNum', 'desc')
            ->setMaxResults(7)
            ->getQuery()
            ->getResult();
    }

    public function topFiveMessageUsers()
    {
        return $query = $this->getEntityManager()
            ->createQueryBuilder()
            ->select('COUNT(m.id) messnum, user.username')
            ->from('TrillalaBundle:Message', 'm')
            ->innerJoin('m.user', 'user')
            ->orderBy('messnum', 'desc')
            ->groupBy('user.username')
            ->setMaxResults(7)
            ->getQuery()
            ->getResult();
    }

    public function countUsersByRegisterDate($date)
    {
        return $query = $this->getEntityManager()
            ->createQueryBuilder()
            ->select('Count(u.id) total, MONTH(u.datetime) mdate, YEAR(u.datetime) ydate')
            ->from('TrillalaBundle:User', 'u')
            ->where('MONTH(u.datetime) <= :mdate')
            ->setParameter('mdate', $date)
            ->groupBy('mdate')
            ->addGroupBy('ydate')
            ->getQuery()
            ->getResult();
    }

    public function getuserCountWithStatus($user){
        return $query = $this->getEntityManager()
            ->createQueryBuilder()
            ->select('Count(u.id) total')
            ->from('TrillalaBundle:User', 'u')
            ->where('u.status is not null')
            ->andWhere('u.enabled = true')
            ->andWhere('u != :user')
            ->setParameter('user', $user)
            ->getQuery()
            ->getResult();
    }

    public function getrandomUserWithStatus($user, $randomNumber){

        return $query = $this->getEntityManager()
            ->createQueryBuilder()
            ->select('u.status, u.username, u.fullname, u.gender, u.avatar')
            ->from('TrillalaBundle:User', 'u')
            ->where('u.status is not null')
            ->andWhere('u.enabled = true')
            ->andWhere('u != :user')
            ->setParameter('user', $user)
            ->setFirstResult($randomNumber)
            ->setMaxResults(1)
            ->getQuery()
            ->getResult();

    }

}