<?php
/**
 * Created by PhpStorm.
 * User: basel
 * Date: 27/10/17
 * Time: 03:37 م
 */

namespace TrillalaBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;
use TrillalaBundle\Entity\Conversation;


class ContactRepository extends EntityRepository
{
    public function getAllContacts($user){
        return $this->getEntityManager()->getRepository('TrillalaBundle:Contact')
            ->createQueryBuilder('c')
            ->select('c.id, user.username contact,user.fullname fullname, user.gender, c.datetime ')
            ->where('c.deleted = false')
            ->andWhere('c.user = :user')
            ->setParameter('user', $user)
            ->innerJoin('c.contact', 'user')
            /*->innerJoin('c.contact', 'u1')*/
            ->orderBy('c.datetime','desc')
            ->getQuery()
            ->getResult();
    }

    public function getAllContactStatus($user){
        return $this->getEntityManager()->getRepository('TrillalaBundle:Contact')
            ->createQueryBuilder('c')
            ->select('c.id, user.username contact,user.fullname fullname, user.status status')
            ->where('c.deleted = false')
            ->andWhere('c.user = :user')
            ->andWhere('user.status  IS NOT NULL')
            ->setParameter('user', $user)
            ->innerJoin('c.contact', 'user')
            /*->innerJoin('c.contact', 'u1')*/
            ->orderBy('c.datetime','desc')
            ->getQuery()
            ->getResult();
    }


    public function countContacts($user){
        return $this->getEntityManager()->getRepository('TrillalaBundle:Contact')
            ->createQueryBuilder('c')
            ->select('COUNT(c.id) total')
            ->where('c.deleted = false')
            ->andWhere('c.user = :user')
            ->setParameter('user', $user)
            ->setMaxResults(1)
            ->getQuery()
            ->getResult();
    }

    public function getContactsUsernames($user){
        return $this->getEntityManager()->getRepository('TrillalaBundle:Contact')
            ->createQueryBuilder('c')
            ->select('user.username')
            ->where('c.deleted = false')
            ->andWhere('c.user = :user')
            ->setParameter('user', $user)
            ->innerJoin('c.contact', 'user')
            ->getQuery()
            ->getResult();
    }

}