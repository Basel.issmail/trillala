<?php
/**
 * Created by PhpStorm.
 * User: basel
 * Date: 11/10/17
 * Time: 03:39 م
 */

namespace TrillalaBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;


class RevenueRepository extends EntityRepository
{
    public function getRevenueSum(){
        return $this->getEntityManager()->createQueryBuilder()
            ->select("SUM(revenue.price) revenues")
            ->from("TrillalaBundle:Revenue", "revenue")
            ->getQuery()->getOneOrNullResult();
    }
}