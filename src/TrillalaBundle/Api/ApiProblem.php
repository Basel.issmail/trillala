<?php
/**
 * Created by PhpStorm.
 * User: basel
 * Date: 07/10/17
 * Time: 11:50 م
 */

namespace TrillalaBundle\Api;

/**
 * A wrapper for holding data to be used for a application/problem+json response
 */
class ApiProblem
{
    private $statusCode;
    private $type;
    private $title;
    private $extraData = array();
    public function __construct($statusCode, $type, $title)
    {
        $this->statusCode = $statusCode;
        $this->type = $type;
        $this->title = $title;
    }
    public function toArray()
    {
        return array_merge(
            $this->extraData,
            array(
                'status' => $this->statusCode,
                'type' => $this->type,
                'title' => $this->title,
            )
        );
    }
    public function set($name, $value)
    {
        $this->extraData[$name] = $value;
    }
    public function getStatusCode()
    {
        return $this->statusCode;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }
}