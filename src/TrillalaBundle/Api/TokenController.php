<?php
/**
 * Created by PhpStorm.
 * User: basel
 * Date: 07/10/17
 * Time: 12:12 ص
 */

namespace TrillalaBundle\Api;

use FOS\RestBundle\View\View;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Exception\BadCredentialsException;
use Lexik\Bundle\JWTAuthenticationBundle\Encoder\JWTEncoderInterface;
use Lexik\Bundle\JWTAuthenticationBundle\TokenExtractor\AuthorizationHeaderTokenExtractor;
use Symfony\Component\Validator\Constraints\DateTime;


class TokenController extends Controller
{
    /**
     * @Route("/login")
     * @Method("POST")
     */
    public function newTokenAction(Request $request)
    {
        $user = $this->getDoctrine()
            ->getRepository('TrillalaBundle:User')
            ->findOneBy(['username' => $request->getUser()]);
        if (!$user) {
            throw $this->createAccessDeniedException();
        }
        $isValid = $this->get('security.password_encoder')
            ->isPasswordValid($user, $request->getPassword());
        if (!$isValid) {
            throw new BadCredentialsException();
        }

        $userInfo = $this->get('user.info');
        $userInfo->setUser($user);
        $response = $userInfo->getUserInfo();
        $response = $userInfo->addTokenInResponse($response);

        return new JsonResponse($response, 200);
 }

    /**
     * @Route("/send")
     * @Method("POST")
     */
    public function newTestAction(Request $request)
    {
        $service = $this->get('syndex.api.firebase');

        $user = $this->getUser();
        //$service->
        $response  = $service->sendNotification("erVNfmfQ3-Q:APA91bF_VviGjmRX45X4uDuxbRUrtfkU3FBnUWFtrJwUUXCsPOmtKJUmp0elwfF0OMCGe3lsOKQ7_1kd2y-dwdVeu2TPHEkExtejnl9Ctkm7KCnSyTTrb5jt33InNc2OZIxYPOpcTVxr", "hello", "test");
        $view = View::create($response, 200);
        return new JsonResponse(['message' => "good", 'status' => 200], 200);
    }
}
