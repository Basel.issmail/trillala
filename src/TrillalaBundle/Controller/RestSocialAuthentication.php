<?php
/**
 * Created by PhpStorm.
 * User: basel
 * Date: 14/11/17
 * Time: 02:09 م
 */


namespace TrillalaBundle\Controller;

use FOS\RestBundle\Controller\Annotations;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\UserBundle\Model\UserInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\View\View;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\ORM\Query;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use TrillalaBundle\Entity\Avatar;
use TrillalaBundle\Entity\Message;
use TrillalaBundle\Entity\Settings;
use TrillalaBundle\Entity\User;
use TrillalaBundle\Model\SocialUser;

//throw new BadRequestHttpException("Content is empty");

class RestSocialAuthentication extends FOSRestController
{

    /**
     * @Annotations\Post("/login/fb")
     */
    public function postSocialAuthAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $at = $request->get('access_token');

        if (!$at) {
            throw new \Exception("Provide FB Access Token");
        }
        $result = $this->getUserByToken($at);
        if ($result === false) {
            $view = View::create(['status' => 'error'], 200);
            $view->setFormat('json');
            return $this->handleView($view);
        }
        if ($result['type'] === 'old_user') {
            $user = $result['data'];
            return $this->getResponse($user, '0', $result['facebookAccount']);
        } else {
            $socialUser = $result['data'];
            $user = new User();
            $user->setUsername($socialUser->getUsername());
            $user->setEmail($socialUser->getEmail());
            $user->setGender($socialUser->getGender());
            $user->setFullname($socialUser->getFullname());
            $user->setEnabled(1);
            $user->setPassword('randomGeneratedToken');
            $em->persist($user);
            $settings = new Settings();
            $settings->setUser($user);
            $settings->setReceiveFromFemale(false);
            $settings->setReceiveFromMale(false);
            $settings->setNotification(true);
            $settings->setReceiveFromCountry(false);
            $em->persist($settings);
            $em->flush();
            return $this->getResponse($user, '1', true);
        }
    }

    private function getUserByToken($at)
    {
        $em = $this->getDoctrine()->getManager();
        $socialUser = $this->getUserInfo($at);
        if ($socialUser === null) {
            return false;
        }
        if ($socialUser->getEmail() === "undefined") {
            $socialUser->setEmail(null);
        }
        $user = null;

        $user = $em->getRepository('TrillalaBundle:User')->findOneByUsername($socialUser->getUsername());
        if ($user !== null) {
            return [
                'type' => 'old_user',
                'data' => $user,
                'facebookAccount' => true
            ];
        }
        $user = $em->getRepository('TrillalaBundle:User')->findOneByFacebookUsername($socialUser->getUsername());
        if ($user !== null) {
            return [
                'type' => 'old_user',
                'data' => $user,
                'facebookAccount' => false
            ];
        }

        return [
            'type' => 'socialuser',
            'data' => $socialUser,
        ];
    }


    private function getUserInfo($at)
    {
        $user = new SocialUser();

        $json_string = @file_get_contents(
            "https://graph.facebook.com/me?fields=email,name,gender,id&access_token=" . $at
        );

        if ($json_string === false) {
            return null;
        }

        //json string to array
        $parsed_arr = json_decode($json_string, true);
        $user->setAccessToken($at);
        $user->setUsername($parsed_arr['id']);
        $user->setFullname($name = str_replace(' ', '_', $parsed_arr['name']));
        if (isset($parsed_arr['email'])) {
            $user->setEmail($parsed_arr['email']);
        } else {
            $user->setEmail("undefined");
        }
        if ($parsed_arr['gender'] == 'male') {
            $user->setGender('m');
        } else {
            $user->setGender('f');
        }
        /* dump($user);die();*/
        return $user;
    }

    private function getResponse(User $user, $newAccount, $fbAccount)
    {
        $userInfo = $this->get('user.info');
        $userInfo->setUser($user);
        $response = $userInfo->getUserInfo();
        $response['facebookAccount'] = $fbAccount;
        $response['newAccount'] = $newAccount;
        $response = $userInfo->addTokenInResponse($response);

        return new JsonResponse( $response, 200);

    }

}