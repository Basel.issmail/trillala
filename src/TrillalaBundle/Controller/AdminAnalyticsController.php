<?php
/**
 * Created by PhpStorm.
 * User: basel
 * Date: 30/10/17
 * Time: 12:15 م
 */

namespace TrillalaBundle\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\Validator\Constraints\DateTime;

class AdminAnalyticsController extends Controller
{

    /**
     * @Route("/admin/trillala/analytics/users", name="user_analytics")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $allUsersCount = $em->getRepository('TrillalaBundle:User')->countAllUsers()[0]['total'];
        $revenue = $em->getRepository('TrillalaBundle:Revenue')->getRevenueSum()['revenues'];
        $revenue = number_format($revenue,3);

        $usersByGender = $this->getUsersDataBySex($allUsersCount);
        $byRegion = $this->getUsersDataByRegion($allUsersCount);
        $countriesByRegion = $byRegion['countries'];
        $usersByRegion = $byRegion['users'];

        $lastyear = new \DateTime('-1year');
        $userByRegisterDate = $this->getUsersDataByRegisterDate($lastyear);
        $registerDates = $userByRegisterDate['date'];
        $registerCountUsers = $userByRegisterDate['users'];

        $lastMonth = new \DateTime('-1month');
        $messageByDate = $this->getMessagesByDate($lastMonth);
        $messageDates = $messageByDate['date'];
        $countMessage = $messageByDate['messages'];
        $allMessagesCount = $em->getRepository('TrillalaBundle:Message')->countAllMessages()[0]['total'];


        $lastDay = new \DateTime('-1day');
        $activeLastDay = $this->getUsersActiveInDate($lastDay, $allUsersCount);
        $topTenConversationUsers = $em->getRepository("TrillalaBundle:User")->topFiveConversationUsers();
        $topTenMessagesUsers = $em->getRepository("TrillalaBundle:User")->topFiveMessageUsers();

        return $this->render('TrillalaBundle:Default:analytics.html.twig', array(
            'allUsersNum' => $allUsersCount,
            'allMessagesNum' => $allMessagesCount,
            'usersByGender' => $usersByGender,
            'activeLastDay' => $activeLastDay,
            'countriesByRegion' => $countriesByRegion,
            'usersByRegion' => $usersByRegion,
            'topTenConversationUsers' => $topTenConversationUsers,
            'registerDates' => $registerDates,
            'registerCountUsers' => $registerCountUsers,
            'topTenMessagesUsers' => $topTenMessagesUsers,
            'messageDates' => $messageDates,
            'countMessage' => $countMessage,
            'revenue' => $revenue
        ));
    }

    private function getMessagesByDate($date)
    {
        $em = $this->getDoctrine()->getManager();
        $usersRegisterCount = $em->getRepository('TrillalaBundle:Message')->countMessageByDate($date);

        $date = [];
        $messageCount = [];
        foreach ($usersRegisterCount as $data){
            array_push($date, $data['ydate'] . '-' . $data['mdate'] . '-' . $data['ddate']);
            array_push($messageCount, $data['total']);
        }

        $data = array('date' => implode(",",$date), 'messages' => implode(",",$messageCount));
        //dump("'as" + implode("','",$countries) + "'");die();
        return $data;
    }

    private function getUsersDataByRegisterDate($date)
    {
        $em = $this->getDoctrine()->getManager();
        $usersRegisterCount = $em->getRepository('TrillalaBundle:User')->countUsersByRegisterDate($date);

        $date = [];
        $userCount = [];
        foreach ($usersRegisterCount as $data){
            array_push($date, $data['ydate'] . '-' . $data['mdate']);
            array_push($userCount, $data['total']);
        }

        $data = array('date' => implode(",",$date), 'users' => implode(",",$userCount));
        //dump("'as" + implode("','",$countries) + "'");die();
        return $data;
    }

    private function getUsersDataBySex($allUsers){
        $em = $this->getDoctrine()->getManager();
        $maleUsers = $em->getRepository('TrillalaBundle:User')->countUsersByGender('m');
        $femaleUsers = $em->getRepository('TrillalaBundle:User')->countUsersByGender('f');
        $data = array($maleUsers[0]['total'], $femaleUsers[0]['total'], $allUsers - ($maleUsers[0]['total'] + $femaleUsers[0]['total']));
        $data = implode(",",$data);

        return $data;
    }

    private function getUsersActiveInDate($date, $allUsers){
        $em = $this->getDoctrine()->getManager();
        $lastDay = $em->getRepository('TrillalaBundle:User')->countUsersByLoginDate($date);
        $data = array($lastDay[0]['total'], $allUsers - $lastDay[0]['total']);
        $data = implode(",",$data);

        return $data;
    }

    private function getUsersDataByRegion($allUsersCount){
        $em = $this->getDoctrine()->getManager();
        $usersByRegion = $em->getRepository('TrillalaBundle:User')->countUsersByRegion();
        $countries = [];
        $userCount = [];
        $total = 0;
        foreach ($usersByRegion as $data){
            array_push($countries, $data['region']);
            array_push($userCount, $data['total']);
            $total += $data['total'];
        }
        array_push($countries, "other");
        array_push($userCount, $allUsersCount - $total);
        $data = array('countries' => implode(",",$countries), 'users' => implode(",",$userCount));
        //dump("'as" + implode("','",$countries) + "'");die();
        return $data;
    }

}