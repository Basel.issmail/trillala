<?php
/**
 * Created by PhpStorm.
 * User: basel
 * Date: 29/10/17
 * Time: 08:15 م
 */

namespace TrillalaBundle\Controller;

use FOS\RestBundle\Controller\Annotations;
use FOS\RestBundle\Controller\FOSRestController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\View\View;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\ORM\Query;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use TrillalaBundle\Entity\Contact;
use TrillalaBundle\Entity\Message;

//throw new BadRequestHttpException("Content is empty");

/**
 * @Security("is_granted('ROLE_USER')")
 */
class RestDeviceController extends FOSRestController
{
    /**
     * @Annotations\Post("/devices")
     */
    public function postGcmTokenAction(Request $request)
    {
        $user = $this->getUser();
        $gcmId = $request->request->get('token');
        if(empty($gcmId)){
            throw new BadRequestHttpException("Content is empty");
        }
        $em = $this->getDoctrine()->getManager();

        $device = $em->getRepository('TrillalaBundle:User')->findOneBy(array('gcmToken' => $gcmId));
        if($device){
            $device->setGcmToken(null);
            $em->persist($device);
            $em->flush();
        }

        $user->setGcmToken($gcmId);
        $em->persist($user);
        $em->flush();
        $sent = array('message' => "gcm Token Saved", 'status' => 200);
        $view = View::create($sent, 200);
        return $this->handleView($view);
    }

}