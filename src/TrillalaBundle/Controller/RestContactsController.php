<?php
/**
 * Created by PhpStorm.
 * User: basel
 * Date: 15/10/17
 * Time: 04:19 م
 */

namespace TrillalaBundle\Controller;

use FOS\RestBundle\Controller\Annotations;
use FOS\RestBundle\Controller\FOSRestController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\View\View;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\ORM\Query;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use TrillalaBundle\Entity\Contact;
use TrillalaBundle\Entity\Message;

//throw new BadRequestHttpException("Content is empty");

/**
 * @Security("is_granted('ROLE_USER')")
 */
class RestContactsController extends FOSRestController
{
    public function getContactsAllAction()
    {
        $user = $this->getUser();
        $em = $this->getDoctrine()->getManager();
        $contacts = $em->getRepository('TrillalaBundle:Contact')->getContactsUsernames($user);
        $conversations = $em->getRepository('TrillalaBundle:Conversation')->getContactConversations($user, $contacts);

        $remainingContacts = $this->getRemainingContacts($user);
        $sent = array('contacts' => $conversations, 'remainingContacts' => $remainingContacts, 'status' => 200);
        $view = View::create($sent, 200);
        return $this->handleView($view);
    }

    /**
     * @Annotations\Post("/contact/add/{username}")
     */
    public function postAddContactAction($username)
    {
        $em = $this->getDoctrine()->getManager();

        $user = $this->getUser();
        $contactToAdd = $em->getRepository('TrillalaBundle:User')->findOneByUsername($username);

        if(!$contactToAdd){
            throw new \Exception("Invalid User");
        }

        $conversation = $em->getRepository('TrillalaBundle:Conversation')->getConversation($user, $contactToAdd, false);



        if (!$conversation) {
            throw $this->createNotFoundException();
        }

        $em = $this->getDoctrine()->getManager();
        $contact = $em->getRepository('TrillalaBundle:Contact')->findOneBy(array('contact' =>  $contactToAdd, "user" => $user ) );

        if ($contact) {
            if ($contact->getDeleted() == true) {
                $contact->setDeleted(0);
                $em->persist($contact);
                $em->flush();
                $remainingContacts = $this->getRemainingContacts($user);
                $contacts = $em->getRepository('TrillalaBundle:Contact')->getAllContacts($user);
                $res = array('username' => $username, 'gender' => $contactToAdd->getGender());
                $reponse = array('contact' => $res, 'message' => 'Added Successfuly', 'contacts' => $contacts, "conversation" => $conversation->getId(), 'remainingContacts' => $remainingContacts, 'status' => 200);
                $view = View::create($reponse, 200);
                return $this->handleView($view);
            }
            $remainingContacts = $this->getRemainingContacts($user);
            $contacts = $em->getRepository('TrillalaBundle:Contact')->getAllContacts($user);
            $reponse = array('contact' => $username, 'contacts' => $contacts, 'message' => 'Already Added', "conversation" => $conversation->getId(), 'remainingContacts' => $remainingContacts, 'status' => 200);
            $view = View::create($reponse, 200);
            return $this->handleView($view);
        }

        $contact = new Contact();
        $contact->setUser($user);
        $contact->setContact($contactToAdd);

        $em->persist($contact);
        $em->flush();
        $remainingContacts = $this->getRemainingContacts($user);
        $contacts = $em->getRepository('TrillalaBundle:Contact')->getAllContacts($user);
        $res = array('username' => $username, 'fullname' => $contactToAdd->getFullname(), 'gender' => $contactToAdd->getGender());

        $reponse = array('contact' => $res, 'message' => 'Added Successfuly', "conversation" => $conversation->getId(), 'contacts' => $contacts, 'remainingContacts' => $remainingContacts, 'status' => 200);
        $view = View::create($reponse, 200);
        return $this->handleView($view);
    }

    /**
     * @Annotations\Post("/contact/delete/{username}")
     */
    public function postDeleteContactAction($username)
    {

        $em = $this->getDoctrine()->getManager();

        $user = $this->getUser();
        $contact =  $em->getRepository('TrillalaBundle:User')->findOneByUsername($username);

        if(!$contact){
            $remainingContacts = $this->getRemainingContacts($user);
            $contacts = $em->getRepository('TrillalaBundle:Contact')->getAllContacts($user);
            $reponse = array('contact' => $username, 'contacts' => $contacts, 'message' => 'contact Already Deleted', 'remainingContacts' => $remainingContacts, 'status' => 200);
            $view = View::create($reponse, 200);
            return $this->handleView($view);
        }

        $contactToAdd = $em->getRepository('TrillalaBundle:Contact')->findOneBy(array( 'user'=> $user, 'contact' => $contact ));

        if(!$contactToAdd){
            $remainingContacts = $this->getRemainingContacts($user);
            $contacts = $em->getRepository('TrillalaBundle:Contact')->getAllContacts($user);
            $reponse = array('contact' => $username, 'contacts' => $contacts, 'message' => 'contact Already Deleted', 'remainingContacts' => $remainingContacts, 'status' => 200);
            $view = View::create($reponse, 200);
            return $this->handleView($view);
        }

        if($contactToAdd->getDeleted() == 1 ){
            $remainingContacts = $this->getRemainingContacts($user);
            $contacts = $em->getRepository('TrillalaBundle:Contact')->getAllContacts($user);
            $reponse = array('contact' => $username, 'contacts' => $contacts, 'message' => 'contact Already Deleted', 'remainingContacts' => $remainingContacts, 'status' => 200);
            $view = View::create($reponse, 200);
            return $this->handleView($view);
        }


        $contactToAdd->setDeleted(1);
        $em->persist($contactToAdd);
        $em->flush();

        $remainingContacts = $this->getRemainingContacts($user);
        $contacts = $em->getRepository('TrillalaBundle:Contact')->getAllContacts($user);
        $reponse = array('contact' => $username, 'contacts' => $contacts, 'message' => 'contact Deleted' , 'remainingContacts' => $remainingContacts, 'status' => 200);
        $view = View::create($reponse, 200);
        return $this->handleView($view);
    }

    private function getRemainingContacts($user){
        $settings = $this->getDoctrine()
            ->getRepository('TrillalaBundle:Settings')->findOneByUser($user);

        $remainingContacts = $this->getDoctrine()
            ->getRepository('TrillalaBundle:Contact')->countContacts($user);

        $remainingContacts = ($settings->getMaxContacts() < 1000 )?  $settings->getMaxContacts()  - $remainingContacts[0]['total'] :  $settings->getMaxContacts();

        return $remainingContacts;
    }
}