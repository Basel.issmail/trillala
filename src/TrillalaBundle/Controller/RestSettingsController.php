<?php
/**
 * Created by PhpStorm.
 * User: basel
 * Date: 29/10/17
 * Time: 11:36 ص
 */

namespace TrillalaBundle\Controller;

use FOS\RestBundle\Controller\Annotations;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Routing\ClassResourceInterface;
use FOS\RestBundle\Controller\Annotations\RouteResource;
use FOS\UserBundle\Event\FilterUserResponseEvent;
use FOS\UserBundle\Event\GetResponseUserEvent;
use FOS\UserBundle\FOSUserEvents;
use FOS\UserBundle\Event\FormEvent;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\View\View;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use TrillalaBundle\Entity\Settings;
use TrillalaBundle\Form\SettingsFormType;
use TrillalaBundle\Model\SocialUser;

/**
 * @RouteResource("settings", pluralize=false)
 * @Security("is_granted('ROLE_USER')")
 */
class RestSettingsController extends FOSRestController
{
    public function putAction(Request $request, $username)
    {
        $user = $this->checkUser($username);
        return $this->updateSettings($request, true, $user);
    }

    public function patchAction(Request $request, $username)
    {
        $user = $this->checkUser($username);
        return $this->updateSettings($request, false, $user);
    }

    /**
     * @param Request $request
     * @param bool $clearMissing
     * @param UserInterface $user
     *
     * @return View|null|\Symfony\Component\Form\FormInterface|Response
     */
    private function updateSettings(Request $request, $clearMissing = true, UserInterface $user)
    {

        $em = $this->getDoctrine()->getManager();
        // just setup a fresh $settings object (remove the dummy data)
        $settings = $em->getRepository('TrillalaBundle:Settings')->findOneByUser($user);

        if (!$settings) {
            $settings = new Settings();
        }
        $oldSettings = clone $settings;

        $form = $this->createForm(SettingsFormType::class, $settings, ['csrf_protection' => false]);
        $form->setData($settings);
        $form->submit($request->request->all(), $clearMissing);

        if (!$form->isValid()) {

            $view = View::create($form);
            return $this->handleView($view);
        }

        $settings->setUser($user);
        $em->persist($settings);
        $em->flush();
        $newSettings = $settings;
        $revenueService = $this->get('app.revenue');
        $revenueService->checkPurchase($oldSettings, $newSettings, $user);
        $facebook = ($user->getFacebookUsername())? true: false;
        $data = array("receiveFromMale" => $settings->getReceiveFromMale(), "receiveFromFemale" => $settings->getReceiveFromFemale(),"facebook" => $facebook, "receiveCountry" => $settings->getReceiveFromCountry(), "notification" => $settings->getNotification(), "maxContacts" => $settings->getMaxContacts(), "maxMessages" => $settings->getMaxMessages(), "extraMessages" => $settings->getExtraMessages(), "maxTime" => $settings->getMaxTime());
        $response = new JsonResponse(['message' => 'updated', 'data' => $data, 'status' => 201], 201);

        return $response;

    }

    private function checkUser($user)
    {
        if ($user !== $this->getUser()->getUsername()) {
            throw new AccessDeniedHttpException();
        }
        return $this->getUser();
    }

    /**
     * @Annotations\Post("/message/extra")
     */
    public function addExtraMessageAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        $extraMessagesNum = $request->get('extraMessage');
        $settings = $this->getDoctrine()
            ->getRepository('TrillalaBundle:Settings')->findOneByUser($user);

        $userCurrentExtra = $settings->getExtraMessages();

        $yesterday = new \DateTime();
        $remainingMessages = $this->getDoctrine()
            ->getRepository('TrillalaBundle:Message')->getRemainingMessages($user, $yesterday);
        $remainingMessages = $settings->getMaxMessages() - $remainingMessages[0]['total'];

        if($extraMessagesNum < 0){
            $allMessages = ($remainingMessages < 0)? $userCurrentExtra : $userCurrentExtra + $remainingMessages;
            $sent = array('message' => 'Added Successfully', 'remainingMessages' => $allMessages, 'status' => 200);
            $view = View::create($sent, 200);
            return $this->handleView($view);
        }

        if (!$extraMessagesNum) {
            $extraMessagesNum = 1;
        }

        $userCurrentExtra += $extraMessagesNum;
        $settings->setExtraMessages($userCurrentExtra);
        $em->persist($settings);
        $em->flush();
        $allMessages = ($remainingMessages < 0)? $userCurrentExtra : $userCurrentExtra + $remainingMessages;
        $sent = array('message' => 'Added Successfully', 'remainingMessages' => $allMessages, 'status' => 200);
        $view = View::create($sent, 200);
        return $this->handleView($view);
    }

    /**
     * @Annotations\Post("/connect/fb")
     */
    public function connectFacebookAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $at = $request->get('access_token');

        if (!$at) {
            $user = $this->getUser();
            $user->setFacebookUsername(null);
            $em->persist($user);
            $em->flush();
            return new JsonResponse(['message' => 'token cleared', 'status' => 200], 200);
        }
        $result = $this->getUserByToken($at);
        if ($result === false) {
            $view = View::create(['status' => 'error'], 200);
            $view->setFormat('json');
            return $this->handleView($view);
        }
        if ($result['type'] === 'old_user') {
            return new JsonResponse(['message' => 'facebook account already registered', 'status' => 200], 200);
        } else {
            $user = $this->getUser();
            $socialUser = $result['data'];
            $user->setFacebookUsername($socialUser->getUsername());
            $em->persist($user);
            $em->flush();
            return new JsonResponse(['message' => 'success', 'status' => 200], 200);
        }
    }

    private function getUserByToken($at)
    {
        $em = $this->getDoctrine()->getManager();
        $socialUser = $this->getUserInfo($at);
        if ($socialUser === null) {
            return false;
        }
        if ($socialUser->getEmail() === "undefined") {
            $socialUser->setEmail(null);
        }
        $user = null;

        $user = $em->getRepository('TrillalaBundle:User')->findOneByUsername($socialUser->getUsername());
        if ($user !== null) {
            return [
                'type' => 'old_user',
                'data' => $user,
            ];
        }


        return [
            'type' => 'socialuser',
            'data' => $socialUser,
        ];
    }

    private function getUserInfo($at)
    {
        $user = new SocialUser();

        $json_string = @file_get_contents(
            "https://graph.facebook.com/me?fields=email,name,gender,id&access_token=" . $at
        );

        if ($json_string === false) {
            return null;
        }

        //json string to array
        $parsed_arr = json_decode($json_string, true);
        $user->setAccessToken($at);
        $user->setUsername($parsed_arr['id']);
        $user->setFullname($name = str_replace(' ', '_', $parsed_arr['name']));
        if (isset($parsed_arr['email'])) {
            $user->setEmail($parsed_arr['email']);
        } else {
            $user->setEmail("undefined");
        }
        if ($parsed_arr['gender'] == 'male') {
            $user->setGender('m');
        } else {
            $user->setGender('f');
        }
        /* dump($user);die();*/
        return $user;
    }

}