<?php
/**
 * Created by PhpStorm.
 * User: basel
 * Date: 10/11/17
 * Time: 11:14 ص
 */


namespace TrillalaBundle\Controller;

use FOS\RestBundle\Controller\Annotations;
use FOS\RestBundle\Controller\FOSRestController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\View\View;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\ORM\Query;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use TrillalaBundle\Entity\Avatar;
use TrillalaBundle\Entity\Message;

//throw new BadRequestHttpException("Content is empty");

/**
 * @Security("is_granted('ROLE_USER')")
 */
class RestAvatarController extends FOSRestController
{

    public function getAvatarAllAction(Request $request){
        $user = $this->getUser();
        $em = $this->getDoctrine()->getManager();
        $purchasedAvatars = $em->getRepository('TrillalaBundle:Avatar')->getAllPurchasedAvatars($user);
        $activeAvatar = $user->getAvatar();

        $sent = array('packages' => $purchasedAvatars,'activeAvatar'=> $activeAvatar, 'status' => 200);
        $view = View::create($sent, 200);
        return $this->handleView($view);

    }

    /**
     * @Annotations\Post("/avatar/active")
     */
    public function postAvatarActiveAction(Request $request){
        $user = $this->getUser();
        $package = $request->get('package');
        $avatar = $request->get('avatar');

        if(!$package){
            throw new BadRequestHttpException("package is empty");
        }

        if(!$avatar){
            throw new BadRequestHttpException("avatar is empty");
        }

        $em = $this->getDoctrine()->getManager();
        $purchasedAvatars = $em->getRepository('TrillalaBundle:Avatar')->findOneBy(array('avatar' => $package, 'user' => $user));

        if(!$purchasedAvatars){
            $sent = array('content' => "You didn't purchase this package",'status' => 200);
            $view = View::create($sent, 200);
            return $this->handleView($view);
        }
        $em = $this->getDoctrine()->getManager();
        $user->setAvatar($package . $avatar);
        $em->persist($user);
        $em->flush();

        $sent = array('content' => 'avatar changed','activeAvatar' => $user->getAvatar(), 'status' => 200);
        $view = View::create($sent, 200);
        return $this->handleView($view);
    }

    /**
     * @Annotations\Post("/avatar/purchase")
     */
    public function postAvatarPurchaseAction(Request $request){
        $user = $this->getUser();
        $package = $request->get('package');
        if(!$package){
            throw new BadRequestHttpException("package is empty");
        }

        $em = $this->getDoctrine()->getManager();
        $purchasedPackages = $em->getRepository('TrillalaBundle:Avatar')->findOneBy(array('avatar' => $package, 'user' => $user));

        if($purchasedPackages){
            $sent = array('content' => "You already purchased this package",'status' => 200);
            $view = View::create($sent, 200);
            return $this->handleView($view);
        }

        $newPackage = new Avatar();
        $newPackage->setUser($user);
        $newPackage->setAvatar($package);
        $em->persist($newPackage);
        //$user->setAvatar($package);
        //$em->persist($user);
        $em->flush();
        $revenueService = $this->get('app.revenue');
        $settings = $em->getRepository('TrillalaBundle:Settings')->findOneByUser($user);
        $revenueService->checkPurchase($settings, $settings, $user, $package);

        $sent = array('content' => 'purchased successfully','activeAvatar' => $user->getAvatar(), 'status' => 200);
        $view = View::create($sent, 200);
        return $this->handleView($view);
    }

}