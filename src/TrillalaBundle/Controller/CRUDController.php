<?php
/**
 * Created by PhpStorm.
 * User: basel
 * Date: 17/11/17
 * Time: 06:50 م
 */


namespace TrillalaBundle\Controller;

use Sonata\AdminBundle\Controller\CRUDController as BaseController;
use Sonata\AdminBundle\Datagrid\ProxyQueryInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class CRUDController extends BaseController
{



    /**
     * @param ProxyQueryInterface $selectedModelQuery
     * @param Request             $request
     *
     * @return RedirectResponse
     */
    public function batchActionEnable(ProxyQueryInterface $selectedModelQuery, Request $request = null)
    {


        $modelManager = $this->admin->getModelManager();


        $selectedModels = $selectedModelQuery->execute();

        $em = $this->getDoctrine()->getManager();
        // do the merge work here

        try {
            foreach ($selectedModels as $selectedModel) {
                $user = $selectedModel->getUser();
                if($user != null) {
                    $user->setEnabled(true);
                    $em->persist($user);
                }
            }

            $em->flush();
        } catch (\Exception $e) {
            $this->addFlash('sonata_flash_error', 'Error, try again...');

            return new RedirectResponse(
                $this->admin->generateUrl('list', array('filter' => $this->admin->getFilterParameters()))
            );
        }

        $this->addFlash('sonata_flash_success', 'Enabled Successfully');

        return new RedirectResponse(
            $this->admin->generateUrl('list', array('filter' => $this->admin->getFilterParameters()))
        );
    }

    /**
     * @param ProxyQueryInterface $selectedModelQuery
     * @param Request             $request
     *
     * @return RedirectResponse
     */
    public function batchActionDisable(ProxyQueryInterface $selectedModelQuery, Request $request = null)
    {


        $modelManager = $this->admin->getModelManager();


        $selectedModels = $selectedModelQuery->execute();

        $em = $this->getDoctrine()->getManager();
        // do the merge work here

        try {
            foreach ($selectedModels as $selectedModel) {
                $user = $selectedModel->getUser();
                if($user != null) {
                    $user->setEnabled(false);
                    $em->persist($user);
                }
            }

            $em->flush();
        } catch (\Exception $e) {
            $this->addFlash('sonata_flash_error', 'Error, try again...');

            return new RedirectResponse(
                $this->admin->generateUrl('list', array('filter' => $this->admin->getFilterParameters()))
            );
        }

        $this->addFlash('sonata_flash_success', 'Enabled Successfully');

        return new RedirectResponse(
            $this->admin->generateUrl('list', array('filter' => $this->admin->getFilterParameters()))
        );
    }


}
