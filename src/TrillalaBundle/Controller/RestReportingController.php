<?php
/**
 * Created by PhpStorm.
 * User: basel
 * Date: 24/03/18
 * Time: 03:13 م
 */

namespace TrillalaBundle\Controller;

use FOS\RestBundle\Controller\Annotations;
use FOS\RestBundle\Controller\FOSRestController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\View\View;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\ORM\Query;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use FOS\UserBundle\Mailer\MailerInterface;
use TrillalaBundle\Entity\Message;


/**
 * @Security("is_granted('ROLE_USER')")
 */
class RestReportingController extends FOSRestController
{

    /**
     * @Annotations\Post("/message/report")
     */
    public function reportMessageAction(Request $request)
    {
        $user = $this->getUser();
        $messageId = $request->get('id');
        $reportMessage = $request->get('report');

        $em = $this->getDoctrine()->getManager();
        $message = $em->getRepository('TrillalaBundle:Message')->findOneBy(array('receiver' => $user, 'id' => $messageId));
        if (!$message) {
            throw new BadRequestHttpException("Message not found");
        }
        $subject = 'Report Message';
        $sent = $this->sendEmail($subject, $reportMessage, $user, $message->getUser());

        if(!$sent){
            $view = View::create(array('status' => 'could not send'), 400);
            return $this->handleView($view);
        }

        $message->setDeleted(1);
        $em->persist($message);
        $em->flush();

        $content = array('content' => 'reported successfully', 'status' => 200);
        $view = View::create($content, 200);
        return $this->handleView($view);
    }

    /**
     * @Annotations\Post("/user/report")
     */
    public function reportUserAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $username = $request->get('username');
        $reportMessage = $request->get('report');
        $user = $this->getUser();
        $contact =  $em->getRepository('TrillalaBundle:User')->findOneByUsername($username);

        $subject = 'Report User';
        $sent = $this->sendEmail($subject, $reportMessage, $user, $username);

        if(!$sent || !$contact){
            $view = View::create(array('status' => 'could not send'), 400);
            return $this->handleView($view);
        }
        $contactToDelete = $em->getRepository('TrillalaBundle:Contact')->findOneBy(array( 'user'=> $user, 'contact' => $contact ));
        $contactToDeleteReverse = $em->getRepository('TrillalaBundle:Contact')->findOneBy(array( 'user'=> $contact, 'contact' => $user ));

        if($contactToDelete != null) {
            $contactToDelete->setDeleted(true);
            $em->persist($contactToDelete);
            $em->flush();
        }
        if($contactToDeleteReverse != null){
            $contactToDeleteReverse->setDeleted(true);
            $em->persist($contactToDeleteReverse);
            $em->flush();
        }
        $remainingContacts = $this->getRemainingContacts($user);
        $contacts = $em->getRepository('TrillalaBundle:Contact')->getAllContacts($user);
        $reponse = array('contact' => $username, 'contacts' => $contacts, 'message' => 'contact reported' , 'remainingContacts' => $remainingContacts, 'status' => 200);
        $view = View::create($reponse, 200);
        return $this->handleView($view);
    }



    public function sendEmail($subject, $message, $reportSender, $reportedUser)
    {
        $message = (new \Swift_Message($subject))
            ->setFrom('basel.issmail@gmail.com')
            ->setTo('basel.issmail@gmail.com')
            ->setBody(
                $this->renderView(
                // templates/emails/registration.html.twig
                    '@Trillala/emails/report.html.twig',
                    array('message' => $message, 'reportSender' => $reportSender, 'reportedUser' => $reportedUser)
                ),
                'text/html'
            )
            /*
             * If you also want to include a plaintext version of the message
            ->addPart(
                $this->renderView(
                    'emails/registration.txt.twig',
                    array('name' => $name)
                ),
                'text/plain'
            )
            */
        ;

        $result = $this->get('mailer')->send($message);

        return $result;
    }

    private function getRemainingContacts($user){
        $settings = $this->getDoctrine()
            ->getRepository('TrillalaBundle:Settings')->findOneByUser($user);
        $remainingContacts = $this->getDoctrine()
            ->getRepository('TrillalaBundle:Contact')->countContacts($user);

        $remainingContacts = ($settings->getMaxContacts() < 1000 )?  $settings->getMaxContacts()  - $remainingContacts[0]['total'] :  $settings->getMaxContacts();

        return $remainingContacts;
    }
}

