<?php
/**
 * Created by PhpStorm.
 * User: basel
 * Date: 12/10/17
 * Time: 12:54 ص
 */

namespace TrillalaBundle\Controller;

use FOS\RestBundle\Controller\Annotations;
use FOS\RestBundle\Controller\FOSRestController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\View\View;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\ORM\Query;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use TrillalaBundle\Entity\Message;

//throw new BadRequestHttpException("Content is empty");

/**
 * @Security("is_granted('ROLE_USER')")
 */
class RestMessageController extends FOSRestController
{
    /**
     * @Annotations\Post("/message/new")
     */
    public function postNewMessageAction(Request $request)
    {
        $user = $this->getUser();
        if ($user === null) {
            //TO DO CHANGE
            return $this->createNotFoundException();
        }
        $em = $this->getDoctrine()->getManager();

        $reciever = $request->get('reciever');


        if (!$reciever) {
            throw $this->createNotFoundException();
        }

        $reciever = $em->getRepository('TrillalaBundle:User')->findOneByUsername($reciever);

        $conversation = $em->getRepository('TrillalaBundle:Conversation')->getConversation($user, $reciever, false);


        if (!$conversation) {
            throw $this->createNotFoundException();
        }

        $voice = $request->files->get('voice');

        if (!$voice) {
            throw $this->createNotFoundException();
        }

        $voicePath = $this->processDocument($voice);

        $message = new Message();
        $message->setUser($user);
        $message->setReceiver($reciever);
        $message->setConversation($conversation);
        $message->setSeen(0);
        $message->setType('voice');
        $message->setContent($voicePath);
        $em->persist($message);
        $em->flush();
        $service = $this->get('syndex.api.firebase');
        $firebaseMessage = array('id' => $message->getId(), 'sender' => $user->getUsername(), 'fullname' => $user->getFullname(), 'gender' => $user->getGender(), 'avatar' => $user->getAvatar(), 'content' => $message->getContent(), 'type' => $message->getType(), 'seen' => $message->getSeen(), 'datetime' => $message->getDatetime());
        if ($service->sendMessage($reciever->getGcmToken(), $firebaseMessage, $user->getFullname()) || true) {

            $userInfo = $this->get('user.info');
            $userInfo->setUser($user);
            $userInfo->tryUseExtraMessages();

            $sent = array('message' => $message->getContent(), 'remainingMessages' => $userInfo->getRemainingMessages(), 'status' => 200);
            $view = View::create($sent, 200);
            return $this->handleView($view);
        }
        $message->setDeleted(1);
        $em->persist($message);
        $em->flush();
        $view = View::create(array('status' => 'could not send'), 400);
        return $this->handleView($view);

    }


    /**
     * @Annotations\Post("/message/random")
     */
    public function postRandomMessageAction(Request $request)
    {

        $user = $this->getUser();
        $em = $this->getDoctrine()->getManager();

        for ($i = 0; $i < 10; $i++) {
            //What option to send message
            $userMessagesCount = $em->getRepository("TrillalaBundle:Message")->CountAllSentMessages($user);
            $sendOption = $userMessagesCount[0]['total'] % 4;

            //pick random number 0..users/2
            $numberOfUsers = $em->getRepository("TrillalaBundle:User")->getLeastFortunateUserCount($user);
            $upperBound = floor(($numberOfUsers[0]['total'] - 1) / 2);
            $randomNumber = mt_rand(0, $upperBound);

            $reciever = null;
            if ($sendOption == 0) {
                //new user... messages number min, last login max
                $reciever = $em->getRepository('TrillalaBundle:User')->getUserToSend($user, 0, 'asc', 'desc');
            } else if ($sendOption == 1) {
                //active user randomly with alot of messages... messages number max, last login max
                $reciever = $em->getRepository('TrillalaBundle:User')->getUserToSend($user, $randomNumber, 'desc', 'desc');
            } else if ($sendOption == 2) {
                //not active user randomly... messages number x, last login min
                $reciever = $em->getRepository('TrillalaBundle:User')->getUserToSend($user, $randomNumber, 'x', 'asc');
            } else if ($sendOption == 3) {
                //active user randomly.. messages number x, last login min
                $reciever = $em->getRepository('TrillalaBundle:User')->getUserToSend($user, $randomNumber, 'x', 'desc');
            }
            if (!$reciever) {
                $sent = array('message' => 'send again', 'status' => 200);
                $view = View::create($sent, 200);
                return $this->handleView($view);
            }

            $reciever = $reciever[0];
            $conversation = $em->getRepository('TrillalaBundle:Conversation')->getConversation($user, $reciever, false);

            if (!$conversation) {
                throw $this->createNotFoundException();
            }

            $voice = $request->files->get('voice');

            if (!$voice) {
                throw $this->createNotFoundException();
            }
            if($i == 0 ){
                $voicePath = $this->processDocument($voice);
            }
            $message = new Message();
            $message->setUser($user);
            $message->setReceiver($reciever);
            $message->setConversation($conversation);
            $message->setSeen(0);
            if($i<9){
                $message->setType('broadvoice');
            } else{
                $message->setType('voice');
            }
            $message->setContent($voicePath);
            $em->persist($message);
            $em->flush();
            $numOfrandomMessages = $reciever->getRandomMessagesReceived();
            $numOfrandomMessages++;
            $reciever->setRandomMessagesReceived($numOfrandomMessages);
            $em->persist($reciever);
            $em->flush();

            $service = $this->get('syndex.api.firebase');
            $firebaseMessage = array('id' => $message->getId(), 'sender' => $user->getUsername(), 'fullname' => $user->getFullname(), 'gender' => $user->getGender(), 'avatar' => $user->getAvatar(), 'content' => $message->getContent(), 'type' => $message->getType(), 'seen' => $message->getSeen(), 'datetime' => $message->getDatetime());
            $service->sendMessage($reciever->getGcmToken(), $firebaseMessage, $firebaseMessage, $user->getFullname());
        }
        if (true) {


            $userInfo = $this->get('user.info');
            $userInfo->setUser($user);
            $userInfo->tryUseExtraMessages();

            $sent = array('message' => $message->getContent(), 'remainingMessages' => $userInfo->getRemainingMessages(), 'status' => 200);
            $view = View::create($sent, 200);
            return $this->handleView($view);
        }
        $message->setDeleted(1);
        $em->persist($message);
        $em->flush();
        $view = View::create(array('status' => 'could not send'), 400);
        return $this->handleView($view);
    }

    /**
     * @Annotations\Post("/message/seen/{id}")
     */
    public function postSeenAction(Request $request, $id)
    {


        $user = $this->getUser();
        $em = $this->getDoctrine()->getManager();
        $message = $em->getRepository('TrillalaBundle:Message')->findOneById($id);

        if (!$message) {
            throw $this->createNotFoundException();
        }

        $conversation = $message->getConversation();

        if ($conversation->getUser0() != $user || $conversation->getUser1() != $user) {
            throw $this->createNotFoundException();
        }

        $message->setSeen(1);
        $em->persist($message);
        $em->flush();

        $sent = array('message' => "marked as seen", 'status' => 200);
        $view = View::create($sent, 200);
        return $this->handleView($view);

    }

    /**
     * @Annotations\Get("/message/{id}/last")
     */
    public function getMessageLastAction(Request $request, $id)
    {
        $conversation = $id;
        $user = $this->getUser();
        $em = $this->getDoctrine()->getManager();
        $message = $em->getRepository('TrillalaBundle:Message')->getConversationLastMessage($user, $conversation);
        $sent = array('message' => $message, 'status' => 200);
        $view = View::create($sent, 200);
        return $this->handleView($view);
    }

    /**
     * @Annotations\Get("/messages/received")
     */
    public function getReceivedMessagesAction(Request $request)
    {

        $page = $request->query->get('page');
        $size = $request->query->get('size');

        if (!$page) {
            $page = 0;
        }
        if (!$size) {
            $size = 5;
        }


        $user = $this->getUser();
        $em = $this->getDoctrine()->getManager();
        $messages = $em->getRepository('TrillalaBundle:Message')->getAllReceivedMessages($user, $page, $size);
        $messageCount = $em->getRepository('TrillalaBundle:Message')->CountAllReceivedMessages($user);
        $pagesCount = ceil($messageCount[0]['total'] / $size);

        $sent = array('messages' => $messages, 'PagesCount' => $pagesCount, 'status' => 200);
        $view = View::create($sent, 200);
        return $this->handleView($view);
    }

    /**
     * @Annotations\Delete("/message")
     */
    public function deleteMessageAction(Request $request)
    {
        $user = $this->getUser();
        $messageId = $request->query->get('id');

        $em = $this->getDoctrine()->getManager();
        $message = $em->getRepository('TrillalaBundle:Message')->findOneBy(array('receiver' => $user, 'id' => $messageId));
        if (!$message) {
            throw new BadRequestHttpException("Message not found");
        }

        $message->setDeleted(1);
        $em->persist($message);
        $em->flush();

        $content = array('content' => 'deleted successfully', 'status' => 200);
        $view = View::create($content, 200);
        return $this->handleView($view);
    }

    private function processDocument(UploadedFile $file)
    {

        $document = $file;

        // Generate a unique name for the document before saving it
        $documentName = md5(uniqid()) . '.mp3';

        // Move the document to the directory where documents are stored
        $documentsDir = $this->container->getParameter('kernel.root_dir') . '/../web/uploads/trillala/voice/';
        $document->move($documentsDir, $documentName);

        return $documentName;
    }

}