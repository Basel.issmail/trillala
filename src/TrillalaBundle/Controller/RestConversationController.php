<?php
/**
 * Created by PhpStorm.
 * User: basel
 * Date: 10/11/17
 * Time: 11:11 ص
 */

namespace TrillalaBundle\Controller;

use FOS\RestBundle\Controller\Annotations;
use FOS\RestBundle\Controller\FOSRestController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\View\View;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\ORM\Query;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use TrillalaBundle\Entity\Message;

//throw new BadRequestHttpException("Content is empty");

/**
 * @Security("is_granted('ROLE_USER')")
 */
class RestConversationController extends FOSRestController
{

    public function getConversationAllAction(Request $request){
        $user = $this->getUser();
        $em = $this->getDoctrine()->getManager();
        $conversations = $em->getRepository('TrillalaBundle:Conversation')->getAllConversation($user);
        $sent = array('conversations' => $conversations, 'status' => 200);
        $view = View::create($sent, 200);
        return $this->handleView($view);

    }

    /**
     * @Annotations\Get("/conversations/received")
     */
    public function getReceivedConversationsAction(Request $request){
        $user = $this->getUser();
        $em = $this->getDoctrine()->getManager();
        $conversations = $em->getRepository('TrillalaBundle:Conversation')->getAllReceivedConversations($user);

        $sent = array('conversations' => $conversations, 'status' => 200);
        $view = View::create($sent, 200);
        return $this->handleView($view);
    }

    /**
     * @Annotations\Get("/conversations/contacts")
     */
    public function getContactConversationsAction(Request $request){
        $user = $this->getUser();
        $em = $this->getDoctrine()->getManager();
        $contacts = $em->getRepository('TrillalaBundle:Contact')->getContactsUsernames($user);
        $conversations = $em->getRepository('TrillalaBundle:Conversation')->getContactConversations($user, $contacts);

        $sent = array('conversations' => $conversations, 'status' => 200);
        $view = View::create($sent, 200);
        return $this->handleView($view);
    }

}