<?php
/**
 * Created by PhpStorm.
 * User: basel
 * Date: 11/10/17
 * Time: 08:04 م
 */

namespace TrillalaBundle\Controller;

use FOS\RestBundle\Context\Context;
use FOS\RestBundle\View\View;
use FOS\RestBundle\Controller\Annotations;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Routing\ClassResourceInterface;
use FOS\RestBundle\Controller\Annotations\RouteResource;
use FOS\UserBundle\Event\FilterUserResponseEvent;
use FOS\UserBundle\Event\GetResponseUserEvent;
use FOS\UserBundle\FOSUserEvents;
use FOS\UserBundle\Event\FormEvent;
use JMS\Serializer\SerializationContext;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\FormTypeInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\Security\Core\User\UserInterface;


/**
 * @RouteResource("profile", pluralize=false)
 * @Security("is_granted('ROLE_USER')")
 */
class RestProfileController extends FOSRestController implements ClassResourceInterface
{

    public function getAction()
    {
        $user = $this->getUser();

        $userInfo = $this->get('user.info');
        $userInfo->setUser($user);
        $response = $userInfo->getUserInfo();
        $response = $userInfo->addTokenInResponse($response);

        return new JsonResponse($response, 200);
    }


    public function putAction(Request $request)
    {
        $user = $this->getUser();
        return $this->updateProfile($request, true, $user);
    }

    public function patchAction(Request $request)
    {
        $user = $this->getUser();
        return $this->updateProfile($request, false, $user);
    }
    /**
     * @param Request       $request
     * @param bool          $clearMissing
     * @param UserInterface $user
     *
     * @return View|null|\Symfony\Component\Form\FormInterface|Response
     */
    private function updateProfile(Request $request, $clearMissing = true, UserInterface $user)
    {
        /** @var $dispatcher \Symfony\Component\EventDispatcher\EventDispatcherInterface */
        $dispatcher = $this->get('event_dispatcher');
        $event = new GetResponseUserEvent($user, $request);
        $dispatcher->dispatch(FOSUserEvents::PROFILE_EDIT_INITIALIZE, $event);
        if (null !== $event->getResponse()) {
            return $event->getResponse();
        }

        /** @var $formFactory \FOS\UserBundle\Form\Factory\FactoryInterface */
        $formFactory = $this->get('fos_user.profile.form.factory');
        $form = $formFactory->createForm(['csrf_protection' => false]);
        $form->setData($user);
        $form->submit($request->request->all(), $clearMissing);
        if (!$form->isValid()) {

            $view = View::create($form);
            return $this->handleView($view);
        }
        /** @var $userManager \FOS\UserBundle\Model\UserManagerInterface */
        $userManager = $this->get('fos_user.user_manager');
        $event = new FormEvent($form, $request);
        $dispatcher->dispatch(FOSUserEvents::PROFILE_EDIT_SUCCESS, $event);
        $userManager->updateUser($user);
        // there was no override
        if (null === $response = $event->getResponse()) {
            $userInfo = $this->get('user.info');
            $userInfo->setUser($user);
            $response = $userInfo->getUserInfo();
            $response = $userInfo->addTokenInResponse($response);

            return new JsonResponse($response, 200);
        }
        // unsure if this is now needed / will work the same
        $dispatcher->dispatch(FOSUserEvents::PROFILE_EDIT_COMPLETED, new FilterUserResponseEvent($user, $request, $response));

        $userInfo = $this->get('user.info');
        $userInfo->setUser($user);
        $response = $userInfo->getUserInfo();
        $response = $userInfo->addTokenInResponse($response);

        return new JsonResponse($response, 200);
    }

   /* private function checkUser($user){
        if ($user !== $this->getUser()->getUsername()) {
            throw new AccessDeniedHttpException();
        }
        return $this->getUser();
    }*/
}