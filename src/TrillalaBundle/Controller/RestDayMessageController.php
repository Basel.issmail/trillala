<?php
/**
 * Created by PhpStorm.
 * User: basel
 * Date: 30/10/17
 * Time: 09:27 م
 */

namespace TrillalaBundle\Controller;

use DoctrineExtensions\Query\Mysql\Date;
use FOS\RestBundle\Controller\Annotations;
use FOS\RestBundle\Controller\FOSRestController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\View\View;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\ORM\Query;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Validator\Constraints\DateTime;
use TrillalaBundle\Entity\Message;

//throw new BadRequestHttpException("Content is empty");

/**
 * @Security("is_granted('ROLE_USER')")
 */
class RestDayMessageController extends FOSRestController
{
    /**
     * @Annotations\Post("/status")
     */
    public function postStatusAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        $status = $request->request->get('status');
        $user->setStatus($status);
        $em->persist($user);
        $em->flush();

        $response = array('message' => 'status updated', 'status' => 200);
        $view = View::create($response, 200);
        return $this->handleView($view);
    }

    /**
     * @Annotations\Get("/status/all")
     */
    public function getAllStatusAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();

        $allStatus = $em->getRepository("TrillalaBundle:Contact")->getAllContactStatus($user);


        $response = array('daymessage' => $allStatus, 'status' => 200);
        $view = View::create($response, 200);
        return $this->handleView($view);
    }

    /**
     * @Annotations\Get("/status/random")
     */
    public function getRandomStatusAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $lastNotification = $em->getRepository('TrillalaBundle:Notification')->getLastNotification();
      //  dump($lastNotification);die();

        if($lastNotification){
            $moment =  new \DateTime();
            $now =  new \DateTime();
            $now->modify('-'. $lastNotification->getShowTimeInMinutes() .'minutes');
            $expiryDate = $lastNotification->getStartTime();
            if($expiryDate >= $now && $moment >= $expiryDate){
                $send =  array('username' => $lastNotification->getSender(), 'status' =>  $lastNotification->getBody());
                $response = array( 'content' => $send, 'status' => 200);
                $view = View::create($response, 200);
                return $this->handleView($view);
            }
        }

        $user = $this->getUser();
        $numberOfUsersWithStatus = $em->getRepository("TrillalaBundle:User")->getuserCountWithStatus($user);
        $randomNumber = mt_rand(0, $numberOfUsersWithStatus[0]['total'] - 1);
        //$randomNumber = 0;
        $randomUserWithStatus = $em->getRepository("TrillalaBundle:User")->getrandomUserWithStatus($user, $randomNumber);


        $response = array( 'content' => $randomUserWithStatus[0], 'status' => 200);
        $view = View::create($response, 200);
        return $this->handleView($view);
    }

}