<?php

namespace TrillalaBundle\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;


class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     * @Method("GET")
     */
    public function indexAction()
    {
        return $this->redirect('http://www.trillala.com/');
        //return $this->render('TrillalaBundle:Default:index.html.twig');
    }

    /**
     * @Route("/api/programmers")
     * @Method("POST")
     */
    public function newAction(Request $request)
    {
        //$this->denyAccessUnlessGranted('ROLE_USER');
        //$programmer = $this->getUser();
        //$programmer = new Programmer();
        //$form = $this->createForm(ProgrammerType::class, $programmer);
        //$this->processForm($request, $form);
        //if (!$form->isValid()) {
        //    $this->throwApiProblemValidationException($form);
        //}
        //$programmer->setUser($this->getUser());
        //$em = $this->getDoctrine()->getManager();
        //$em->persist($programmer);
        //$em->flush();
        //$response = $this->createApiResponse("hello", 200);
        /*$programmerUrl = $this->generateUrl(
            'api_programmers_show',
            ['nickname' => $programmer->getNickname()]
        );*/
        //$response = $this->json(['status'=> 200]);
        //$response->headers->set('Location', $programmerUrl);
        //return $response;
        return new JsonResponse([
            'user' => 'user you'
        ], 201);
    }

    /**
     * @Route("/", name="fos_user_profile_show")
     * @Method("GET")
     */
    public function confirmChangedPasswordAction(Request $request)
    {
        return $this->redirect('http://www.trillala.com/');
        /*$this->addFlash(
            'notice',
            'Your Password is changed!'
        );
        return $this->redirectToRoute('homepage');*/
    }
}
