<?php
/**
 * Created by PhpStorm.
 * User: basel
 * Date: 11/10/17
 * Time: 05:03 م
 */
namespace TrillalaBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\View\View;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\ORM\Query;
use Doctrine\Common\Collections\ArrayCollection;


/**
 * @Security("is_granted('ROLE_USER')")
 */
class RestNotificationController  extends FOSRestController
{

    public function getNotificationLastAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $lastNotification = $em->getRepository('TrillalaBundle:Notification')->getLastNotification();

        $view = View::create($lastNotification,200);
        return $this->handleView($view);
    }

    public function getNotificationNewAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $lastNotification = $em->getRepository('TrillalaBundle:Notification')->getNewNotifications();

        $view = View::create($lastNotification,200);
        return $this->handleView($view);
    }
}