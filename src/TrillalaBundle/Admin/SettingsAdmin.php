<?php
/**
 * Created by PhpStorm.
 * User: basel
 * Date: 17/11/17
 * Time: 06:12 م
 */


namespace TrillalaBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Symfony\Component\Form\Extension\Core\Type\EmailType;

class SettingsAdmin extends AbstractAdmin
{
    public function getExportFields()
    {
        return array('user.fullname','user.email', 'user.gender','user.enabled', 'maxTime','maxContacts', 'maxMessages', 'user.country');
    }

    public function getBatchActions()
    {
        // retrieve the default (currently only the delete action) actions
        $actions = parent::getBatchActions();

          $actions['enable']=[
                'label'            => $this->trans('enable', array(), 'SonataAdminBundle'),
                'ask_confirmation' => true
            ];

        $actions['disable']=[
            'label'            => $this->trans('disable', array(), 'SonataAdminBundle'),
            'ask_confirmation' => true // If true, a confirmation will be asked before performing the action
        ];

        return $actions;
    }

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->remove('create');
    }

    protected function configureFormFields(FormMapper $formMapper)
    {

        $formMapper->add('user.enabled', 'checkbox')
            ->add('maxTime')
            ->add('maxMessages')
            ->add('maxContacts');
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('user.fullname')
            ->add('maxTime')
        ->add('maxMessages')
        ->add('maxContacts')
        ->add('user.country')
            ->add('user.gender')
        ->add('user.enabled');
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('user.username')
            ->add('user.fullname')
            ->add('user.email')
            ->add('user.enabled', null, array(
                'editable' => true
            ))
            ->add('maxTime', null, array(
                'editable' => true
            ))
            ->add('maxMessages', null, array(
                'editable' => true
            ))
            ->add('maxContacts', null, array(
                'editable' => true
            ))
            ->add('user.lastLogin')
            ->add('user.gender')
            ->add('user.randomMessagesReceived')
            ->add('user.country');
    }
}