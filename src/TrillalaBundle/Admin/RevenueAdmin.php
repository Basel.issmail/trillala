<?php
/**
 * Created by PhpStorm.
 * User: basel
 * Date: 20/10/17
 * Time: 05:54 م
 */

namespace TrillalaBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Symfony\Component\Form\Extension\Core\Type\EmailType;

class RevenueAdmin extends AbstractAdmin
{
    public function getExportFields()
    {
        return array('username', 'details', 'price', 'purchaseTime');
    }

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->remove('create')
            ->remove('delete');
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        //$formMapper->add('enabled');
        //->add('email', 'text');
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('user.username')
            ->add('details');
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->add('user.fullname')
            ->add('details')
            ->add('price')
            ->add('purchaseTime');
    }
}