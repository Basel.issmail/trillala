<?php
/**
 * Created by PhpStorm.
 * User: basel
 * Date: 20/10/17
 * Time: 07:20 م
 */

namespace TrillalaBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;

class NotificationAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper->add('sender', 'text')
        ->add('body')->add('showTimeInMinutes')->add('startTime');
    }

/*    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->remove('create')->remove('delete');
    }*/

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('body')
        ->add('sender')
        ->add('showTimeInMinutes')
            ->add('startTime');
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('sender')
        ->add('body')
        ->add('showTimeInMinutes')
            ->add('startTime');
    }

    public function createQuery($context = 'list')
    {
        $query = parent::createQuery($context);
        $query->andWhere('o.deleted = 0');
        return $query;
    }

}