<?php
/**
 * Created by PhpStorm.
 * User: basel
 * Date: 14/11/17
 * Time: 02:25 م
 */


namespace TrillalaBundle\Model;


class SocialUser implements \Serializable
{
    private $email;
    private $gender;
    private $fullname;
    private $id;
    private $accessToken;

    public function serialize()
    {
        return serialize([
            $this->email,
            $this->gender,
            $this->fullname,
            $this->username,
            $this->accessToken,
        ]);
    }

    public function unserialize($serialized)
    {
        list(
            $this->email,
            $this->gender,
            $this->fullname,
            $this->username,
            $this->accessToken,

            ) = unserialize($serialized);
    }


    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * @param mixed $gender
     */
    public function setGender($gender)
    {
        $this->gender = $gender;
    }

    /**
     * @return mixed
     */
    public function getFullname()
    {
        return $this->fullname;
    }

    /**
     * @param mixed $fullname
     */
    public function setFullname($fullname)
    {
        $this->fullname = $fullname;
    }

    /**
     * @return mixed
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param mixed $username
     */
    public function setUsername($username)
    {
        $this->username = $username;
    }

    /**
     * @return string
     */
    public function getAccessToken()
    {
        return $this->accessToken;
    }

    /**
     * @param string $accessToken
     */
    public function setAccessToken($accessToken)
    {
        $this->accessToken = $accessToken;
    }

}