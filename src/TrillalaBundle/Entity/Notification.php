<?php
/**
 * Created by PhpStorm.
 * User: basel
 * Date: 11/10/17
 * Time: 03:36 م
 */

namespace TrillalaBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\User as BaseUser;

/**
 * Notification
 *
 * @ORM\Table(name="mobile_notification")
 * @ORM\Entity(repositoryClass="TrillalaBundle\Repository\NotificationRepository")
 */
class Notification
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string $sender
     *
     * @ORM\Column(name="sender", type="string", length=100 ,unique=false, nullable=false)
     *
     */
    private $sender = "admin";

    /**
     * @var string $body
     *
     * @ORM\Column(name="body", type="string", length=255 ,unique=false, nullable=false)
     *
     */
    private $body;

    /**
     * @var int
     *
     * @ORM\Column(name="show_time_in_minutes", type="integer")
     *
     */
    protected $showTimeInMinutes = 1;

    /**
     * @var bool $deleted
     *
     * @ORM\Column(name="deleted", type="boolean" ,unique=false, nullable=false)
     *
     */
    private $deleted = false;

    /**
     * @var \DateTime $startTime
     *
     * @ORM\Column(name="startTime", type="datetime" ,unique=false, nullable=false)
     *
     */
    private $startTime;


    /**
     * @ORM\ManyToOne(targetEntity="TrillalaBundle\Entity\User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;


    public function __construct()
    {
        $this->startTime = new \DateTime('now');
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set sender
     *
     * @param string $sender
     *
     * @return Notification
     */
    public function setSender($sender)
    {
        $this->sender = $sender;

        return $this;
    }

    /**
     * Get sender
     *
     * @return string
     */
    public function getSender()
    {
        return $this->sender;
    }

    /**
     * Set body
     *
     * @param string $body
     *
     * @return Notification
     */
    public function setBody($body)
    {
        $this->body = $body;

        return $this;
    }

    /**
     * string body
     *
     * @return string
     */
    public function getBody()
    {
        return $this->body;
    }


    /**
     * Set deleted
     *
     * @param boolean $deleted
     *
     * @return Notification
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;

        return $this;
    }

    /**
     * Get deleted
     *
     * @return boolean
     */
    public function getDeleted()
    {
        return $this->deleted;
    }

    /**
     * Set startTime
     *
     * @param \DateTime $startTime
     *
     * @return Notification
     */
    public function setStartTime($startTime)
    {
        $this->startTime = $startTime;

        return $this;
    }

    /**
     * Get startTime
     *
     * @return \DateTime
     */
    public function getStartTime()
    {
        return $this->startTime;
    }

    /**
     * Set user
     *
     * @param \TrillalaBundle\Entity\User $user
     *
     * @return Notification
     */
    public function setUser(User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \TrillalaBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set showTimeInMinutes
     *
     * @param integer $showTimeInMinutes
     *
     * @return Notification
     */
    public function setShowTimeInMinutes($showTimeInMinutes)
    {
        $this->showTimeInMinutes = $showTimeInMinutes;

        return $this;
    }

    /**
     * Get showTimeInMinutes
     *
     * @return integer
     */
    public function getShowTimeInMinutes()
    {
        return $this->showTimeInMinutes;
    }
}
