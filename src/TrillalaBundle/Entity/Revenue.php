<?php
/**
 * Created by PhpStorm.
 * User: basel
 * Date: 11/10/17
 * Time: 03:36 م
 */

namespace TrillalaBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\User as BaseUser;

/**
 * Notification
 *
 * @ORM\Table(name="revenue")
 * @ORM\Entity(repositoryClass="TrillalaBundle\Repository\RevenueRepository")
 */
class Revenue
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string $details
     *
     * @ORM\Column(name="details", type="string", length=255 ,unique=false, nullable=false)
     *
     */
    private $details;

    /**
     * @var float
     *
     * @ORM\Column(name="price", type="float")
     *
     */
    protected $price = 0;

    /**
     * @var bool $deleted
     *
     * @ORM\Column(name="deleted", type="boolean" ,unique=false, nullable=false)
     *
     */
    private $deleted = false;

    /**
     * @var \DateTime $purchaseTime
     *
     * @ORM\Column(name="purchaseTime", type="datetime" ,unique=false, nullable=false)
     *
     */
    private $purchaseTime;


    /**
     * @ORM\ManyToOne(targetEntity="TrillalaBundle\Entity\User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;


    public function __construct()
    {
        $this->purchaseTime = new \DateTime('now');
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set $details
     *
     * @param string $details
     *
     * @return Revenue
     */
    public function setDetails($details)
    {
        $this->details = $details;

        return $this;
    }

    /**
     * string details
     *
     * @return string
     */
    public function getDetails()
    {
        return $this->details;
    }


    /**
     * Set deleted
     *
     * @param boolean $deleted
     *
     * @return Revenue
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;

        return $this;
    }

    /**
     * Get deleted
     *
     * @return boolean
     */
    public function getDeleted()
    {
        return $this->deleted;
    }

    /**
     * Set startTime
     *
     * @param \DateTime $purchaseTime
     *
     * @return Revenue
     */
    public function setPurchaseTime($purchaseTime)
    {
        $this->purchaseTime = $purchaseTime;

        return $this;
    }

    /**
     * Get startTime
     *
     * @return \DateTime
     */
    public function getPurchaseTime()
    {
        return $this->purchaseTime;
    }

    /**
     * Set user
     *
     * @param \TrillalaBundle\Entity\User $user
     *
     * @return Revenue
     */
    public function setUser(User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \TrillalaBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set price
     *
     * @param integer $price
     *
     * @return Revenue
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return integer
     */
    public function getPrice()
    {
        return $this->price;
    }
}
