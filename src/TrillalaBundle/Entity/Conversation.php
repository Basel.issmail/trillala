<?php
/**
 * Created by PhpStorm.
 * User: basel
 * Date: 12/10/17
 * Time: 12:46 ص
 */

namespace TrillalaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="TrillalaBundle\Repository\ConversationRepository")
 * @ORM\Table(name="message_conversation")
 */
class Conversation {
    /**
     * @ORM\Id
     * @ORM\Column(type="guid")
     * @ORM\GeneratedValue(strategy="UUID")
     */
    private $id;


    /**
     * @ORM\ManyToOne(targetEntity="TrillalaBundle\Entity\User")
     * @ORM\JoinColumn(name="user0_id", referencedColumnName="id",onDelete="CASCADE")
     */
    private $user0;

    /**
     * @ORM\ManyToOne(targetEntity="TrillalaBundle\Entity\User")
     * @ORM\JoinColumn(name="user1_id", referencedColumnName="id",onDelete="CASCADE")
     */
    private $user1;

    /**
     * @ORM\Column(type="string", nullable=true, options={"default":"shamra"})
     */
    private $app;

    /**
     * @ORM\Column(type="boolean", nullable=false, options={"default":0})
     */
    private $active = false;

    /**
     * @ORM\Column(type="datetime", nullable=false)
     */
    private $datetime;

    /**
     * One Conversation has Many Messages.
     * @ORM\OneToMany(targetEntity="TrillalaBundle\Entity\Message", mappedBy="conversation")
     */
    private $messages;

    public function __construct()
    {
        $this->datetime = new \DateTime();
    }

    /**
     * Get id
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set app
     *
     * @param string $app
     *
     * @return Conversation
     */
    public function setApp($app)
    {
        $this->app = $app;

        return $this;
    }

    /**
     * Get app
     *
     * @return string
     */
    public function getApp()
    {
        return $this->app;
    }

    /**
     * Set active
     *
     * @param boolean $active
     *
     * @return Conversation
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set user0
     *
     * @param \TrillalaBundle\Entity\User $user0
     *
     * @return Conversation
     */
    public function setUser0(User $user0 = null)
    {
        $this->user0 = $user0;

        return $this;
    }

    /**
     * Get user0
     *
     * @return \TrillalaBundle\Entity\User
     */
    public function getUser0()
    {
        return $this->user0;
    }

    /**
     * Set user1
     *
     * @param \TrillalaBundle\Entity\User $user1
     *
     * @return Conversation
     */
    public function setUser1(User $user1 = null)
    {
        $this->user1 = $user1;

        return $this;
    }

    /**
     * Get user1
     *
     * @return \TrillalaBundle\Entity\User
     */
    public function getUser1()
    {
        return $this->user1;
    }

    /**
     * Set datetime
     *
     * @param integer $datetime
     * @return Conversation
     */
    public function setDatetime($datetime)
    {
        $this->datetime = $datetime;

        return $this;
    }

    /**
     * Get datetime
     *
     * @return \DateTime
     */
    public function getDatetime()
    {
        return $this->datetime;
    }
}
