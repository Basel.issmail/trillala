<?php
/**
 * Created by PhpStorm.
 * User: basel
 * Date: 27/10/17
 * Time: 03:31 م
 */

namespace TrillalaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="TrillalaBundle\Repository\ContactRepository")
 * @ORM\Table(name="contact")
 */
class Contact {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


    /**
     * @ORM\ManyToOne(targetEntity="TrillalaBundle\Entity\User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id",onDelete="CASCADE")
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity="TrillalaBundle\Entity\User")
     * @ORM\JoinColumn(name="contact_id", referencedColumnName="id",onDelete="CASCADE")
     */
    private $contact;

    /**
     * @ORM\Column(type="datetime", nullable=false)
     */
    private $datetime;

    /**
     * @var bool $deleted
     *
     * @ORM\Column(name="deleted", type="boolean" ,unique=false, nullable=false)
     *
     */
    private $deleted = 0;


    public function __construct()
    {
        $this->datetime = new \DateTime();
    }


    /**
     * Get id
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set datetime
     *
     * @param integer $datetime
     * @return Contact
     */
    public function setDatetime($datetime)
    {
        $this->datetime = $datetime;

        return $this;
    }

    /**
     * Get datetime
     *
     * @return \DateTime
     */
    public function getDatetime()
    {
        return $this->datetime;
    }

    /**
     * Set user
     *
     * @param \TrillalaBundle\Entity\User $user
     * @return Contact
     */
    public function setUser(User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \TrillalaBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }


    /**
     * Set contact
     *
     * @param \TrillalaBundle\Entity\User $user
     * @return Contact
     */
    public function setContact(User $contact = null)
    {
        $this->contact = $contact;

        return $this;
    }

    /**
     * Get contact
     *
     * @return \TrillalaBundle\Entity\User
     */
    public function getContact()
    {
        return $this->contact;
    }

    /**
     * Set deleted
     *
     * @param boolean $deleted
     *
     * @return Contact
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;

        return $this;
    }

    /**
     * Get deleted
     *
     * @return boolean
     */
    public function getDeleted()
    {
        return $this->deleted;
    }

}
