<?php
/**
 * Created by PhpStorm.
 * User: basel
 * Date: 29/10/17
 * Time: 11:03 ص
 */

namespace TrillalaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="TrillalaBundle\Repository\SettingsRepository")
 * @ORM\Table(name="settings")
 */
class Settings {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


    /**
     * @ORM\OneToOne(targetEntity="TrillalaBundle\Entity\User", inversedBy="settings", orphanRemoval=true)
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;

    /**
     * @var bool $receiveFromMale
     *
     * @ORM\Column(name="receive_from_male", type="boolean" ,unique=false, nullable=false)
     *
     */
    private $receiveFromMale = false;

    /**
     * @var bool $receiveFromFemale
     *
     * @ORM\Column(name="receive_from_female", type="boolean" ,unique=false, nullable=false)
     *
     */
    private $receiveFromFemale = false;

    /**
     * @var bool $receiveFromCountry
     *
     * @ORM\Column(name="receive_from_country", type="boolean" ,unique=false, nullable=false)
     *
     */
    private $receiveFromCountry = false;

    /**
     * @var bool $notification
     *
     * @ORM\Column(name="notification", type="boolean" ,unique=false, nullable=false)
     *
     */
    private $notification = true;

    /**
     * @var int
     *
     * @ORM\Column(name="max_time", type="integer")
     *
     */
    private $maxTime = 6;

    /**
     * @var int
     *
     * @ORM\Column(name="max_messages", type="integer")
     *
     */
    private $maxMessages = 4;

    /**
     * @var int
     *
     * @ORM\Column(name="extra_messages", type="integer")
     *
     */
    private $extraMessages = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="max_contacts", type="integer")
     *
     */
    private $maxContacts = 6;


    public function __construct()
    {

    }


    /**
     * Get id
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set user
     *
     * @param \TrillalaBundle\Entity\User $user
     * @return Settings
     */
    public function setUser(User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \TrillalaBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set receiveFromMale
     *
     * @param boolean $receiveFromMale
     *
     * @return Settings
     */
    public function setReceiveFromMale($receiveFromMale)
    {
        $this->receiveFromMale = $receiveFromMale;

        return $this;
    }

    /**
     * Get receiveFromMale
     *
     * @return boolean
     */
    public function getReceiveFromMale()
    {
        return $this->receiveFromMale;
    }


    /**
     * Set receiveFromFemale
     *
     * @param boolean $receiveFromFemale
     *
     * @return Settings
     */
    public function setReceiveFromFemale($receiveFromFemale)
    {
        $this->receiveFromFemale = $receiveFromFemale;

        return $this;
    }

    /**
     * Get receiveFromCountry
     *
     * @return boolean
     */
    public function getReceiveFromCountry()
    {
        return $this->receiveFromCountry;
    }

    /**
     * Set receiveFromCountry
     *
     * @param boolean $receiveFromCountry
     *
     * @return Settings
     */
    public function setReceiveFromCountry($receiveFromCountry)
    {
        $this->receiveFromCountry = $receiveFromCountry;

        return $this;
    }

    /**
     * Get receiveFromFemale
     *
     * @return boolean
     */
    public function getReceiveFromFemale()
    {
        return $this->receiveFromFemale;
    }


    /**
     * Set notification
     *
     * @param boolean $notification
     *
     * @return Settings
     */
    public function setNotification($notification)
    {
        $this->notification = $notification;

        return $this;
    }

    /**
     * Get notification
     *
     * @return boolean
     */
    public function getNotification()
    {
        return $this->notification;
    }

    /**
     * Set maxTime
     *
     * @param integer $maxTime
     *
     * @return Settings
     */
    public function setMaxTime($maxTime)
    {
        $this->maxTime = $maxTime;

        return $this;
    }

    /**
     * Get maxTime
     *
     * @return integer
     */
    public function getMaxTime()
    {
        return $this->maxTime;
    }


    /**
     * Set maxMessages
     *
     * @param integer $maxMessages
     *
     * @return Settings
     */
    public function setMaxMessages($maxMessages)
    {
        $this->maxMessages = $maxMessages;

        return $this;
    }

    /**
     * Get maxMessages
     *
     * @return integer
     */
    public function getMaxMessages()
    {
        return $this->maxMessages;
    }

    /**
     * Set extraMessages
     *
     * @param integer $extraMessages
     *
     * @return Settings
     */
    public function setExtraMessages($extraMessages)
    {
        $this->extraMessages = $extraMessages;

        return $this;
    }

    /**
     * Get extraMessages
     *
     * @return integer
     */
    public function getExtraMessages()
    {
        return $this->extraMessages;
    }

    /**
     * Set maxContacts
     *
     * @param integer $maxContacts
     *
     * @return Settings
     */
    public function setMaxContacts($maxContacts)
    {
        $this->maxContacts = $maxContacts;

        return $this;
    }

    /**
     * Get maxContacts
     *
     * @return integer
     */
    public function getMaxContacts()
    {
        return $this->maxContacts;
    }

}
