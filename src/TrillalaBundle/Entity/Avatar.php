<?php
/**
 * Created by PhpStorm.
 * User: basel
 * Date: 09/11/17
 * Time: 02:24 م
 */

namespace TrillalaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="TrillalaBundle\Repository\AvatarRepository")
 * @ORM\Table(name="avatar")
 */
class Avatar {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


    /**
     * @ORM\ManyToOne(targetEntity="TrillalaBundle\Entity\User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;

    /**
     * @var string
     *
     * @ORM\Column(name="avatar", type="string", nullable=true)
     */
    private $avatar;


    public function __construct()
    {

    }


    /**
     * Get id
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set user
     *
     * @param \TrillalaBundle\Entity\User $user
     * @return Settings
     */
    public function setUser(User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \TrillalaBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set avatar
     *
     * @param string $avatar
     * @return Avatar
     */
    public function setAvatar($avatar)
    {
        $this->avatar = $avatar;

        return $this;
    }

    /**
     * Get avatar
     *
     * @return string
     */
    public function getAvatar()
    {
        return $this->avatar;
    }

}
