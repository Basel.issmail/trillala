<?php
/**
 * Created by PhpStorm.
 * User: basel
 * Date: 06/10/17
 * Time: 08:59 م
 */

namespace TrillalaBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\User as BaseUser;
/**
 * @ORM\Entity(repositoryClass="TrillalaBundle\Repository\UserRepository")
 * @ORM\Table(name="`user`")
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="gender", type="string", nullable=true)
     */
    protected $gender;

    /**
     * @var int
     *
     * @ORM\Column(name="conversation_num", type="integer")
     *
     */
    protected $conversationNum = 0 ;

    /**
     * @var int
     *
     * @ORM\Column(name="random_messages_received", type="integer")
     *
     */
    protected $randomMessagesReceived = 0 ;

    /**
     * @var string
     *
     * @ORM\Column(name="facebook_username", type="string", nullable=true)
     */
    protected $facebookUsername;

    /**
     * @var string
     *
     * @ORM\Column(name="gcm_token", type="string", nullable=true)
     */
    protected $gcmToken;

    /**
     * One User has One Setting.
     * @ORM\OneToOne(targetEntity="TrillalaBundle\Entity\Settings", mappedBy="user")
     */
    protected $settings;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $avatar;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", nullable=true)
     */
    protected $status;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $datetime;

    /**
     * @var string
     *
     * @ORM\Column(name="country_code", type="string", nullable=true)
     */
    protected $country;

    /**
     * @var string
     *
     * @ORM\Column(name="full_name", type="string", nullable=true)
     */
    protected $fullname;

    public function __construct()
    {
        parent::__construct();
        $this->datetime = new \DateTime();
    }

    public function getId()
    {
        return $this->id;
    }

    /**
     * Set conversationNum
     *
     * @param integer $conversationNum
     *
     * @return User
     */
    public function setConversationNum($conversationNum)
    {
        $this->conversationNum = $conversationNum;

        return $this;
    }

    /**
     * Get randomMessagesReceived
     *
     * @return integer
     */
    public function getRandomMessagesReceived()
    {
        return $this->randomMessagesReceived;
    }

    /**
     * Set conversationNum
     *
     * @param integer $randomMessagesReceived
     *
     * @return User
     */
    public function setRandomMessagesReceived($randomMessagesReceived)
    {
        $this->randomMessagesReceived = $randomMessagesReceived;

        return $this;
    }

    /**
     * Get conversationNum
     *
     * @return integer
     */
    public function getConversationNum()
    {
        return $this->conversationNum;
    }

    /**
     * Set gender
     *
     * @param string $gender
     * @return User
     */
    public function setGender($gender)
    {
        $this->gender = $gender;

        return $this;
    }

    /**
     * Get gender
     *
     * @return string
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * Set facebookUsername
     *
     * @param string $facebookUsername
     * @return User
     */
    public function setFacebookUsername($facebookUsername)
    {
        $this->facebookUsername = $facebookUsername;

        return $this;
    }

    /**
     * Get facebookUsername
     *
     * @return string
     */
    public function getFacebookUsername()
    {
        return $this->facebookUsername;
    }

    /**
     * Set gcmToken
     *
     * @param string $gcmToken
     * @return User
     */
    public function setGcmToken($gcmToken)
    {
        $this->gcmToken = $gcmToken;

        return $this;
    }

    /**
     * Get gcmToken
     *
     * @return string
     */
    public function getGcmToken()
    {
        return $this->gcmToken;
    }

    /**
     * Set avatar
     *
     * @param string $avatar
     * @return User
     */
    public function setAvatar($avatar)
    {
        $this->avatar = $avatar;

        return $this;
    }

    /**
     * Get avatar
     *
     * @return string
     */
    public function getAvatar()
    {
        return $this->avatar;
    }

    /**
     * Set status
     *
     * @param string $status
     * @return User
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set country
     *
     * @param string $country
     * @return User
     */
    public function setCountry($country)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }


    /**
     * Set fullname
     *
     * @param string $fullname
     * @return User
     */
    public function setFullname($fullname)
    {
        $this->fullname = $fullname;

        return $this;
    }

    /**
     * Get fullname
     *
     * @return string
     */
    public function getFullname()
    {
        return $this->fullname;
    }

}