<?php
/**
 * Created by PhpStorm.
 * User: basel
 * Date: 12/10/17
 * Time: 12:45 ص
 */

namespace TrillalaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="TrillalaBundle\Repository\MessageRepository")
 * @ORM\Table(name="message",
 *     indexes={
 *          @ORM\Index(name="message_datetime_idx", columns={"datetime"})
 *     }
 * )
 */
class Message {

    /**
     * @ORM\Id
     * @ORM\Column(type="guid")
     * @ORM\GeneratedValue(strategy="UUID")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=400, nullable=false)
     */
    private $content;

    /**
     * @ORM\ManyToOne(targetEntity="TrillalaBundle\Entity\User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id",onDelete="CASCADE")
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity="TrillalaBundle\Entity\User")
     * @ORM\JoinColumn(name="receiver_id", referencedColumnName="id",onDelete="CASCADE")
     */
    private $receiver;


    /**
     * @ORM\ManyToOne(targetEntity="TrillalaBundle\Entity\Conversation", inversedBy="messages")
     * @ORM\JoinColumn(name="conversation_id", referencedColumnName="id",onDelete="CASCADE")
     */
    private $conversation;

    /**
     * @ORM\Column(type="datetime", nullable=false)
     */
    private $datetime;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $seen = null;

    /**
     * @var bool $deleted
     *
     * @ORM\Column(name="deleted", type="boolean" ,unique=false, nullable=true)
     *
     */
    private $deleted = false;

    /**
     * @ORM\Column(type="string", nullable=true, options={"default":"text"})
     */
    private $type;


    public function __construct()
    {
        $this->datetime = new \DateTime();
    }


    /**
     * Get id
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set content
     *
     * @param string $content
     * @return Message
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set datetime
     *
     * @param integer $datetime
     * @return Message
     */
    public function setDatetime($datetime)
    {
        $this->datetime = $datetime;

        return $this;
    }

    /**
     * Get datetime
     *
     * @return \DateTime
     */
    public function getDatetime()
    {
        return $this->datetime;
    }

    /**
     * Set seen
     *
     * @param integer $seen
     * @return Message
     */
    public function setSeen($seen)
    {
        $this->seen = $seen;

        return $this;
    }

    /**
     * Get seen
     *
     * @return integer
     */
    public function getSeen()
    {
        return $this->seen;
    }

    /**
     * Set type
     *
     * @param string $type
     * @return Message
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set user
     *
     * @param \TrillalaBundle\Entity\User $user
     * @return Message
     */
    public function setUser(User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \TrillalaBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set receiver
     *
     * @param \TrillalaBundle\Entity\User $receiver
     * @return Message
     */
    public function setReceiver(User $receiver = null)
    {
        $this->receiver = $receiver;

        return $this;
    }

    /**
     * Get receiver
     *
     * @return \TrillalaBundle\Entity\User
     */
    public function getReceiver()
    {
        return $this->receiver;
    }

    /**
     * Set conversation
     *
     * @param \TrillalaBundle\Entity\Conversation $conversation
     * @return Message
     */
    public function setConversation(Conversation $conversation = null)
    {
        $this->conversation = $conversation;

        return $this;
    }

    /**
     * Get conversation
     *
     * @return \TrillalaBundle\Entity\Conversation
     */
    public function getConversation()
    {
        return $this->conversation;
    }

    /**
     * Set deleted
     *
     * @param boolean $deleted
     *
     * @return Message
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;

        return $this;
    }

    /**
     * Get deleted
     *
     * @return boolean
     */
    public function getDeleted()
    {
        return $this->deleted;
    }
}
