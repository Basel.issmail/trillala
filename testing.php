
<?php
/**
 * Created by PhpStorm.
 * User: basel
 * Date: 20/09/17
 * Time: 11:56 ص
 */

require __DIR__.'/vendor/autoload.php';
$client = new \GuzzleHttp\Client([
    'base_uri' => 'http://localhost/',
    'defaults' => [
        'exceptions' => false,
    ]
]);

$nickname = 'ObjectOrienter'.rand(0, 999);
$data = array(
    'nickname' => $nickname,
    'avatarNumber' => 5,
    'tagLine' => 'a test dev!'
);

$res = $client->request('POST', 'trillala/web/app_dev.php/api/home',[
    'body' => json_encode($data)
]);

echo $res->getStatusCode();
echo "\n";
echo $res->getBody();
echo "\n\n";