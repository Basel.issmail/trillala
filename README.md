First version of Trillala App ([speak me](https://play.google.com/store/apps/details?id=com.storexweb.speakme&hl=en&gl=US))
========================

Trillala App backend APIs, built with symfony framework.

What's Trillala/Speakme?
--------------

SpeakMe is an innovative social, free, where you can listen to the real voice of each user and exchange audio messages, no more boring chats, immediately listen to the voice of the users that interest you.

SpeakMe features:

- Listen to the real voice of the person you are interested in
- Send an audio message to a specific user
- Send your audio message to 5 random people, draw attention to you
- Contact up to 50 people per day with random audio
- Watch the people you contact change during the day for new friends
- Request friendship from those who interest you and become Speak's friends
- Block unwanted people in one click
- See how many visits your profile has
- Do you want to make yourself known? Connect your Instagram account
- The use of SpeakMe is free, you will pay, if you want, only extra features
- Search for people within a specific radius of you, meet them in person
- Are you interested in knowing about a specific country? No problem sets the nation from the VIP and displays only people from that nation.
